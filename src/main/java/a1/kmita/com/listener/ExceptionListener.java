package a1.kmita.com.listener;

public abstract class ExceptionListener{

  public abstract void onError(int code);
  public abstract void onSuccess();
}
