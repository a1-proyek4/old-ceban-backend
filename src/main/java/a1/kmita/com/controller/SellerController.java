package a1.kmita.com.controller;

import java.util.List;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.IOException;
import java.io.FileNotFoundException;

import a1.kmita.com.model.get.Seller;
import a1.kmita.com.model.get.Adv;
import a1.kmita.com.model.get.Notif;
import a1.kmita.com.model.post.LoginPost;
import a1.kmita.com.model.Response;
import a1.kmita.com.model.get.InboxGet;
import a1.kmita.com.model.ResponseGet;
import a1.kmita.com.model.ResponsePost;

import a1.kmita.com.controller.ImageHandler;

@RestController
@RequestMapping("/")
public class SellerController {

@ApiOperation(value = "postSellerLogin", nickname = "post Seller Login")
@RequestMapping(method = RequestMethod.POST, path = "sellers/login",
                consumes = "application/json", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success", response = Seller.class),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public ResponseGet<Seller> postSellerLogin(@RequestBody LoginPost loginPost){
        ResponseGet<Seller> response = new ResponseGet();
        DbHandler db = new DbHandler();
        if(db.getResult() == 0) {
                try{
                        String uid_query = "";
                        try{
                                int slrNim = Integer.parseInt(loginPost.getSlrId());
                                uid_query = "seller.slr_nim = " + loginPost.getSlrId();
                        }catch(NumberFormatException e) {
                                uid_query = "seller.slr_email = '" + loginPost.getSlrId() + "'";
                        }
                        String sql = "SELECT * FROM seller WHERE "+ uid_query +" AND (seller.slr_pass = '"+ loginPost.getSlrPass() +"') LIMIT 1;";
                        ResultSet rs = db.executeQuery(sql);
                        if(rs.next()) {
                                Seller seller = new Seller();
                                seller.setSlrNim(rs.getInt("slr_nim"));
                                seller.setSlrName(rs.getString("slr_name"));
                                seller.setSlrDepartment(rs.getString("slr_department"));
                                seller.setSlrStudyProgram(rs.getString("slr_studyprogram"));
                                seller.setSlrGender(rs.getString("slr_gender").equals("true"));
                                seller.setSlrEmail(rs.getString("slr_email"));
                                seller.setSlrContact(rs.getString("slr_contact"));
                                seller.setSlrImage(rs.getString("slr_img"));
                                response.setData(seller);
                                response.setStatus("0");
                        }else{
                                response.setStatus("3");
                        }
                        response.setSize(rs.getRow());
                        rs.close();
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        System.out.println(e.getMessage());
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}

@ApiOperation(value = "getSellers", nickname = "get Sellers")
@RequestMapping(method = RequestMethod.GET, path = "sellers", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success", response = Seller.class),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public ResponseGet<List<Seller> > getSellers(
        @RequestParam(name="q", defaultValue="", required = false) String query,
        @RequestParam(name="f", defaultValue="seller.slr_nim", required = false) String field,
        @RequestParam(name="s", defaultValue="ASC", required = false) String sort,
        @RequestParam(name="p", defaultValue="1", required = false) int page,
        @RequestParam(name="l", defaultValue="20", required = false) int limit){
        ResponseGet<List<Seller> > response = new ResponseGet();
        List<Seller> list = new ArrayList();

        int i = (limit * (page-1));
        int n = (limit * page);

        DbHandler db = new DbHandler();
        if(db.getResult() == 0) {
                try{
                        String sql = "SELECT * FROM seller WHERE (seller.slr_name like '%" + query + "%') ORDER BY "+ field +" "+ sort +", seller.slr_nim, seller.slr_name ASC LIMIT "+limit+" OFFSET "+i+";";
                        ResultSet rs = db.executeQuery(sql);
                        while(rs.next()) {
                                Seller seller = new Seller();
                                seller.setSlrNim(rs.getInt("slr_nim"));
                                seller.setSlrName(rs.getString("slr_name"));
                                seller.setSlrDepartment(rs.getString("slr_department"));
                                seller.setSlrStudyProgram(rs.getString("slr_studyprogram"));
                                seller.setSlrGender(rs.getString("slr_gender").equals("true"));
                                seller.setSlrEmail(rs.getString("slr_email"));
                                seller.setSlrContact(rs.getString("slr_contact"));
                                seller.setSlrImage(rs.getString("slr_img"));
                                list.add(seller);
                        }
                        if(list.size()>0) {
                                response.setData(list);
                                response.setStatus("0");
                        }else{
                                response.setStatus("3");
                        }
                        rs.close();
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        System.out.println(e.getMessage());
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}


@ApiOperation(value = "getSellerByNim", nickname = "get Seller by Nim")
@RequestMapping(method = RequestMethod.GET, path = "sellers/{slr_nim}", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success", response = Seller.class),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public ResponseGet<Seller> getSellerByNim(@PathVariable int slr_nim){
        ResponseGet<Seller> response = new ResponseGet();

        DbHandler db = new DbHandler();
        if(db.getResult() == 0) {
                try{
                        String sql = "SELECT * FROM seller WHERE slr_nim = '"+ slr_nim +"' LIMIT 1;";
                        ResultSet rs = db.executeQuery(sql);
                        if(rs.next()) {
                                Seller seller = new Seller();
                                seller.setSlrNim(rs.getInt("slr_nim"));
                                seller.setSlrName(rs.getString("slr_name"));
                                seller.setSlrDepartment(rs.getString("slr_department"));
                                seller.setSlrStudyProgram(rs.getString("slr_studyprogram"));
                                seller.setSlrGender(rs.getString("slr_gender").equals("true"));
                                seller.setSlrEmail(rs.getString("slr_email"));
                                seller.setSlrContact(rs.getString("slr_contact"));
                                seller.setSlrImage(rs.getString("slr_img"));
                                response.setData(seller);
                        }
                        response.setSize(rs.getRow());
                        response.setStatus("0");
                        rs.close();
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        System.out.println(e.getMessage());
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}


@ApiOperation(value = "postSellerUpdateImage", nickname = "post Seller Update Image")
@RequestMapping(method = RequestMethod.POST, path = "sellers/update_image",
                consumes = "multipart/form-data", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success"),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public Response postSellerUpdateImage(
        @RequestPart(name = "slrNim", required = true) String slrNim,
        @RequestPart(name = "slrImage", required = true) MultipartFile slrImage){
        Response response = new Response();
        DbHandler db = new DbHandler();
        ImageHandler imageHandler = null;
        if(slrImage != null) {
                imageHandler = new ImageHandler();
        }
        if(db.getResult() == 0) {
                try{
                        String uploadedUrl = "";
                        if(imageHandler != null) {
                                uploadedUrl = imageHandler.upload(slrImage);
                        }
                        String sql = "UPDATE seller SET slr_img = '"+ uploadedUrl +"' WHERE slr_nim = '" + slrNim + "';";
                        if(db.executeUpdate(sql) > 0) {
                                response.setStatus("0");
                        }else{
                                response.setStatus("3");
                        }
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        System.out.println(e.getMessage());
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }catch(FileNotFoundException e) {
                        response.setStatus("4");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }catch(IOException e) {
                        response.setStatus("5");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;

}

@ApiOperation(value = "postSellerRegister", nickname = "post Seller Register")
@RequestMapping(method = RequestMethod.POST, path = "sellers/register",
                consumes = "application/json", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success"),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public ResponsePost postSellerRegister(@RequestBody Seller sellerPost){
        ResponsePost response = new ResponsePost();
        DbHandler db = new DbHandler();
        if(db.getResult() == 0) {
                try{
                        String sql = "INSERT INTO seller (slr_nim, slr_name, slr_department, slr_studyprogram, slr_gender, slr_email, slr_contact, slr_img, slr_pass) VALUES ('"+ sellerPost.getSlrNim() +"', '"+ sellerPost.getSlrName() + "', '"+ sellerPost.getSlrDepartment() +"', '"+ sellerPost.getSlrStudyProgram() +"', "+ sellerPost.isSlrGender() +", '"+ sellerPost.getSlrEmail() +"', '" + sellerPost.getSlrContact() +"', '"+ sellerPost.getSlrImage() +"', '"+ sellerPost.getSlrPass() +"') RETURNING slr_nim;";
                        ResultSet rs = db.executeQuery(sql);
                        if(rs.next()) {
                                response.setId(rs.getInt("slr_nim"));
                                response.setStatus("0");
                        }else{
                                response.setStatus("3");
                        }
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        System.out.println(e.getMessage());
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}


@ApiOperation(value = "getSellerInboxs", nickname = "get Seller Inboxs")
@RequestMapping(method = RequestMethod.GET, path = "sellers/{slr_nim}/inboxs", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success", response = InboxGet.class),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public ResponseGet<List<InboxGet> > getSellerInboxs(
        @PathVariable String slr_nim,
        @RequestParam(name="q", defaultValue="", required = false) String query,
        @RequestParam(name="f", defaultValue="inbox.ibx_id", required = false) String field,
        @RequestParam(name="s", defaultValue="DESC", required = false) String sort,
        @RequestParam(name="p", defaultValue="1", required = false) int page,
        @RequestParam(name="l", defaultValue="10", required = false) int limit){
        ResponseGet<List<InboxGet> > response = new ResponseGet();
        List<InboxGet> list = new ArrayList();

        int i = (limit * (page-1));
        int n = (limit * page);

        DbHandler db = new DbHandler();
        if(db.getResult() == 0) {
                try{
                        String sql = "SELECT * FROM inbox WHERE (inbox.ibx_msg like '%" + query + "%' OR inbox.ibx_sdr_name like '%" + query + "%' OR inbox.ibx_sdr_email like '%" + query + "%') ORDER BY "+ field +" "+ sort +", inbox.ibx_id ASC LIMIT "+limit+" OFFSET "+i+";";
                        ResultSet rs = db.executeQuery(sql);
                        while(rs.next()) {
                                InboxGet inbox = new InboxGet();
                                inbox.setIbxId(rs.getInt("ibx_id"));
                                inbox.setIbxMsg(rs.getString("ibx_msg"));
                                inbox.setIbxSdrEmail(rs.getString("ibx_sdr_email"));
                                inbox.setIbxSdrName(rs.getString("ibx_sdr_name"));
                                inbox.setIbxSdDate(rs.getString("ibx_sd_date"));
                                list.add(inbox);
                        }
                        response.setSize(rs.getRow());
                        response.setStatus("0");
                        response.setData(list);
                        rs.close();
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        System.out.println(e.getMessage());
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}

@ApiOperation(value = "getSellerAds", nickname = "get Seller Ads")
@RequestMapping(method = RequestMethod.GET, path = "sellers/{slr_nim}/ads", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success", response = Adv.class),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public ResponseGet<List<Adv> > getSellerAds(
        @PathVariable String slr_nim,
        @RequestParam(name="q", defaultValue="", required = false) String query,
        @RequestParam(name="f", defaultValue="advert.ad_id", required = false) String field,
        @RequestParam(name="s", defaultValue="DESC", required = false) String sort,
        @RequestParam(name="p", defaultValue="1", required = false) int page,
        @RequestParam(name="l", defaultValue="10", required = false) int limit,
        @RequestParam(name="v", defaultValue="true", required = false) boolean verified){
        ResponseGet<List<Adv> > response = new ResponseGet();
        List<Adv> list = new ArrayList();

        int i = (limit * (page-1));
        int n = (limit * page);

        DbHandler db = new DbHandler();
        if(db.getResult() == 0) {
                try{
                        String sql = "SELECT advert.ad_id, advert.ad_title, advert.ad_price, (SELECT image.image_img FROM image WHERE image.ad_id = advert.ad_id LIMIT 1), advert.slr_nim, seller.slr_name, seller.slr_department FROM advert INNER JOIN seller ON advert.slr_nim = seller.slr_nim WHERE (advert.ad_title like '%" + query + "%' OR advert.ad_desc like '%" + query + "%') AND ad_stat = "+ verified +" AND advert.slr_nim = "+ slr_nim +"  ORDER BY "+ field +" "+ sort +", advert.ad_bump DESC LIMIT "+limit+" OFFSET "+i+" ;";
                        ResultSet rs = db.executeQuery(sql);
                        while(rs.next()) {
                                Adv ad = new Adv();
                                ad.setAdId(rs.getInt("ad_id"));
                                ad.setAdTitle(rs.getString("ad_title"));
                                ad.setAdPrice(rs.getInt("ad_price"));
                                ad.setAdImage(rs.getString("image_img"));
                                ad.setSlrNim(rs.getInt("slr_nim"));
                                ad.setSlrName(rs.getString("slr_name"));
                                ad.setSlrDepartment(rs.getString("slr_department"));
                                list.add(ad);
                        }
                        response.setSize(rs.getRow());
                        response.setStatus("0");
                        response.setData(list);
                        rs.close();
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        System.out.println(e.getMessage());
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}

@ApiOperation(value = "verifySeller", nickname = "verify Seller")
@RequestMapping(method = RequestMethod.POST, path = "sellers/verify",
                consumes = "application/json", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success"),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public Response verifySeller(@RequestBody Seller seller){
        Response response = new Response();
        DbHandler db = new DbHandler();
        if(db.getResult() == 0) {
                try{
                        String sql = "UPDATE seller SET slr_stat = true WHERE slr_nim = "+seller.getSlrNim()+";";
                        if(!db.execute(sql)) {
                                response.setStatus("0");
                        }else{
                                response.setStatus("3");
                        }
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}


@ApiOperation(value = "postEditSeller", nickname = "Post Edit Seller")
@RequestMapping(method = RequestMethod.POST, path = "sellers/edit",
                consumes = "application/json", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success"),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public  Response postEditSeller(@RequestBody Seller slr){
        Response response = new Response();
        DbHandler db = new DbHandler();
        if(db.getResult() == 0) {
                try{
                        String sql = "UPDATE seller SET " + (slr.getSlrContact() == null ? "" : ("slr_contact = '"+slr.getSlrContact()+"', ")) + (slr.getSlrDepartment() == null ? "" : ("slr_department = '"+slr.getSlrDepartment()+"', ")) + (slr.getSlrEmail() == null ? "" : ("slr_email = '"+slr.getSlrEmail()+"', ")) + (slr.getSlrImage() == null ? "" : ("slr_img = '"+slr.getSlrImage()+"', ")) + (slr.getSlrPass() == null ? "" : ("slr_pass = '"+slr.getSlrPass()+"',")) + (slr.getSlrStudyProgram() == null ? "" : ("slr_studyprogram = '"+slr.getSlrStudyProgram()+"',")) + (slr.isSlrGender() == true ? "" : ("slr_gender = '"+slr.isSlrGender()+"', ")) + (slr.getSlrName() == null ? "" : ("slr_name = '"+slr.getSlrName()+"' ")) + " WHERE slr_nim = '"+slr.getSlrNim()+"';";
                        if(!db.execute(sql)) {
                                response.setStatus("0");
                        }else{
                                response.setStatus("3");
                        }
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}

@ApiOperation(value = "getSellerNotif", nickname = "get seller Notif")
@RequestMapping(method = RequestMethod.GET, path = "sellers/{slr_nim}/notif", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success", response = Adv.class),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })

        public ResponseGet<List<Notif>> getSellerNotif(@PathVariable int slr_nim){
                ResponseGet<List<Notif>> response = new ResponseGet();
                DbHandler db = new DbHandler();
                List<Notif> list = new ArrayList();
                if(db.getResult() == 0) {
                        try{
                        String sql = "SELECT * FROM notif WHERE notif.slr_nim = "+slr_nim+";";
                        ResultSet rs = db.executeQuery(sql);
                        while(rs.next()){
                                Notif ntf = new Notif();
                                ntf.setNotifId(rs.getInt("notif_id"));
                                ntf.setSlrNim(rs.getInt("slr_nim"));
                                ntf.setNotifRead(rs.getBoolean("notif_read"));
                                ntf.setNotifMessage(rs.getString("notif_message"));
                                ntf.setNotifTime(rs.getString("notif_time"));
                                list.add(ntf);
                        }
                        response.setSize(rs.getRow());
                        response.setStatus("0");
                        response.setData(list);
                        rs.close();
                        db.closeStatement();
                        db.closeConnection();
                        }catch(SQLException e) {
                                System.out.println(e.getMessage());
                                response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                        }
                }else{
                        response.setStatus(String.valueOf(db.getResult()));
                }
                return response;                        
        }
}
