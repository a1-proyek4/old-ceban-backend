package a1.kmita.com.controller;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.IOException;
import java.io.FileNotFoundException;

import a1.kmita.com.model.get.Report;
import a1.kmita.com.model.ResponseGet;


@RestController
@RequestMapping("/")
public class ReportController {

@ApiOperation(value = "getReport", nickname = "get All Report")
@RequestMapping(method = RequestMethod.GET, path = "reports", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success", response = Report.class),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public ResponseGet<List<Report> > getAllReport(
        @RequestParam(name="q", defaultValue="", required = false) String query,
        @RequestParam(name="f", defaultValue="report.rpt_id", required = false) String field,
        @RequestParam(name="s", defaultValue="DESC", required = false) String sort,
        @RequestParam(name="p", defaultValue="1", required = false) int page,
        @RequestParam(name="l", defaultValue="10", required = false) int limit){
        ResponseGet<List<Report> > response = new ResponseGet();
        List<Report> list = new ArrayList();

        int i = (limit * (page-1));
        int n = (limit * page);
        DbHandler db = new DbHandler();
        if(db.getResult() == 0) {

            try{
                        String sql = "SELECT report.rpt_id, report.ad_id, report.rpt_name, report.rpt_email, report.rpt_cmt, report.rpt_cause, report.rpt_date, advert.ad_title FROM report INNER JOIN advert ON advert.ad_id = report.ad_id WHERE (advert.ad_title like '%" + query + "%' OR report.rpt_name like '%" + query + "%' OR report.rpt_cause like '%" + query + "%') ORDER BY "+ field +" "+ sort +", report.rpt_date DESC LIMIT "+limit+" OFFSET "+i+" ";
                        ResultSet rs = db.executeQuery(sql);
                        while(rs.next()) {
                                Report report = new Report();
                                report.setRptId(rs.getInt("rpt_id"));
                                report.setAdId(rs.getInt("ad_id"));
                                report.setAdTitle(rs.getString("ad_title"));
                                report.setRptName(rs.getString("rpt_name"));
                                report.setRptEmail(rs.getString("rpt_email"));
                                report.setRptCmt(rs.getString("rpt_cmt"));
                                report.setRptCause(rs.getString("rpt_cause"));  
                                report.setRptDate(rs.getString("rpt_date"));                                                                                              
                                list.add(report);
                        }
                        response.setSize(rs.getRow());
                        response.setStatus("0");
                        response.setData(list);
                        rs.close();
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        System.out.println(e.getMessage());
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}

}
