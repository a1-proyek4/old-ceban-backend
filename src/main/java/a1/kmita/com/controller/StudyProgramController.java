package a1.kmita.com.controller;

import java.util.List;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.IOException;
import java.io.FileNotFoundException;

import a1.kmita.com.model.Response;
import a1.kmita.com.model.ResponseGet;
import a1.kmita.com.model.get.StudyProgram;

@RestController
@RequestMapping("/")
public class StudyProgramController {

@ApiOperation(value = "getStudyProgram", nickname = "get StudyProgram")
@RequestMapping(method = RequestMethod.GET, path = "studyprogram", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success", response = StudyProgram.class),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public ResponseGet<List<StudyProgram>> getStudyProgram(){
        ResponseGet<List<StudyProgram>> response = new ResponseGet();
        List<StudyProgram> list = new ArrayList();
        DbHandler db = new DbHandler();
        if(db.getResult() == 0) {
                try{
                        String sql = "SELECT * FROM studyprogram  ORDER BY studyprogram.std_id ASC;";
                        ResultSet rs = db.executeQuery(sql);
                        while(rs.next()) {
                                StudyProgram prodi = new StudyProgram();
                                prodi.setStudyProgramId(rs.getInt("std_id"));
                                prodi.setStudyProgramName(rs.getString("std_name"));
                                list.add(prodi);
                        }
                        if(list.size()>0) {
                                response.setData(list);
                                response.setStatus("0");
                        }else{
                                response.setStatus("3");
                        }
                        rs.close();
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        System.out.println(e.getMessage());
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}

//==================== Get StudyProgram By Id ===================//
@ApiOperation(value = "getStudyProgramById", nickname = "get Study Program By Id")
@RequestMapping(method = RequestMethod.GET, path = "studyprogram/{std_id}", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success", response = StudyProgram.class),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public ResponseGet StudyProgramById(@PathVariable int std_id){
        ResponseGet<StudyProgram> response = new ResponseGet();
        StudyProgram prodi = new StudyProgram();
        DbHandler db = new DbHandler();
        if(db.getResult() == 0) {
                try{
                        //ResultSet rs = db.executeQuery("SELECT promo_id, promo_image, promo_link, promo_dsc, promo_create_time, promo_title, promo_end_time, promo_type FROM promo where promo_id = '" + promo_id + "';");
                       ResultSet rs = db.executeQuery("SELECT * FROM studyprogram where std_id = '" + std_id + "';");
                        if(rs.next()) {
                                prodi.setStudyProgramId(rs.getInt("std_id"));
                                prodi.setStudyProgramName(rs.getString("std_name"));
                        }
                        response.setSize(rs.getRow());
                        response.setStatus("0");
                        response.setData(prodi);
                        rs.close();
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        System.out.println(e.getMessage());
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}

@ApiOperation(value = "StudyProgramByDeptId", nickname = "get Study Program By Dept Id")
@RequestMapping(method = RequestMethod.GET, path = "department/{dpt_id}/studyprogram", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success", response = StudyProgram.class),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public ResponseGet<List<StudyProgram>> StudyProgramByDeptId(@PathVariable int dpt_id){
        ResponseGet<List<StudyProgram>> response = new ResponseGet();
        List<StudyProgram> list = new ArrayList();
        DbHandler db = new DbHandler();
        if(db.getResult() == 0) {
                try{
                        //ResultSet rs = db.executeQuery("SELECT promo_id, promo_image, promo_link, promo_dsc, promo_create_time, promo_title, promo_end_time, promo_type FROM promo where promo_id = '" + promo_id + "';");
                       ResultSet rs = db.executeQuery("SELECT * FROM studyprogram where dpt_id = '" + dpt_id + "';");
                        while(rs.next()) {
                                StudyProgram prodi = new StudyProgram();
                                prodi.setStudyProgramId(rs.getInt("std_id"));
                                prodi.setStudyProgramName(rs.getString("std_name"));
                                list.add(prodi);
                        }
                        if(list.size()>0) {
                                response.setData(list);
                                response.setStatus("0");
                        }else{
                                response.setStatus("3");
                        }
                        rs.close();
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        System.out.println(e.getMessage());
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}


}
