package a1.kmita.com.controller;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.IOException;
import java.io.FileNotFoundException;

import a1.kmita.com.model.post.InboxPost;
import a1.kmita.com.model.Response;
import a1.kmita.com.listener.ExceptionListener;
import a1.kmita.com.model.ResponseGet;

@RestController
@RequestMapping("/")
public class InboxController {

@ApiOperation(value = "postInbox", nickname = "post Inbox")
@RequestMapping(method = RequestMethod.POST, path = "inboxs/new",
                consumes = "application/json", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success"),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public Response postInbox (@RequestBody InboxPost inboxPost){
        Response response = new Response();
        DbHandler db = new DbHandler();

        if(db.getResult() == 0) {
                try{
                        // Date datetoday = new Date(System.currentTimeMillis());
                        // String today = new SimpleDateFormat("yyyy-MM-dd").format(datetoday);
                        String sql = "INSERT INTO inbox (slr_nim, ibx_msg, ibx_sdr_email, ibx_sdr_name, ibx_sd_date) VALUES ('"+inboxPost.getSlrNim()+"', '"+inboxPost.getIbxMsg()+"', '"+inboxPost.getIbxSdrEmail()+"', '"+ inboxPost.getIbxSdrName() +"', NOW());";
                        if (!db.execute(sql)) {
                                response.setStatus("0");
                        }else{
                                response.setStatus("3");
                        }
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}

}
