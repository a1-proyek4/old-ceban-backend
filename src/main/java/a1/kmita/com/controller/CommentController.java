package a1.kmita.com.controller;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.IOException;
import java.io.FileNotFoundException;

import a1.kmita.com.model.post.CommentPost;
import a1.kmita.com.model.get.CommentGet;
import a1.kmita.com.model.Response;
import a1.kmita.com.listener.ExceptionListener;
import a1.kmita.com.model.ResponseGet;

@RestController
@RequestMapping("/")
public class CommentController {

@ApiOperation(value = "postAdsComment", nickname = "post Ads Comment")
@RequestMapping(method = RequestMethod.POST, path = "comments/new",
                consumes = "application/json", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success"),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public Response postAdsComment (@RequestBody CommentPost commentPost){
        Response response = new Response();
        DbHandler db = new DbHandler();

        if(db.getResult() == 0) {
                try{
                        // Date datetoday = new Date(System.currentTimeMillis());
                        // String today = new SimpleDateFormat("yyyy-MM-dd").format(datetoday);
                        String sql = "INSERT INTO comment (ad_id, cmt_msg, cmt_email, cmt_date, cmt_stat) VALUES ('"+commentPost.getAdId()+"', '"+commentPost.getCmtMsg()+"', '"+commentPost.getCmtEmail()+"', NOW(), false);";
                        if (!db.execute(sql)) {
                                response.setStatus("0");
                        }else{
                                response.setStatus("3");
                        }
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}

@ApiOperation(value = "getCommentbyId", nickname = "get Comment by Id")
@RequestMapping(method = RequestMethod.GET, path = "comments/{cmt_id}", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success"),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public ResponseGet<CommentGet> getCommentbyId(@PathVariable int cmt_id){
        ResponseGet<CommentGet> response = new ResponseGet();
        DbHandler db = new DbHandler();
        if(db.getResult() == 0) {
                try{
                        String sql = "SELECT cmt_id, cmt_msg, cmt_email, cmt_date, cmt_stat FROM comment WHERE cmt_id = "+cmt_id+";";
                        ResultSet rs = db.executeQuery(sql);
                        if(rs.next()) {
                                CommentGet cmt = new CommentGet();
                                cmt.setCmtId(rs.getInt("cmt_id"));
                                cmt.setCmtMsg(rs.getString("cmt_msg"));
                                cmt.setCmtEmail(rs.getString("cmt_email"));
                                cmt.setCmtDate(rs.getString("cmt_date"));
                                cmt.setCmtStatus(rs.getBoolean("cmt_stat"));
                                response.setData(cmt);

                        }
                        response.setSize(rs.getRow());
                        response.setStatus("0");
                        rs.close();
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        System.out.println(e.getMessage());
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}

@ApiOperation(value = "verifyComment", nickname = "verify Comment")
@RequestMapping(method = RequestMethod.POST, path = "comments/verify",
                consumes = "application/json", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success"),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public Response verifyComment(@RequestBody CommentGet comment){
        Response response = new Response();
        DbHandler db = new DbHandler();
        if(db.getResult() == 0) {
                try{
                        String sql = "UPDATE comment SET cmt_stat = true WHERE cmt_id = "+comment.getCmtId()+";";
                        if(!db.execute(sql)) {
                                response.setStatus("0");
                        }else{
                                response.setStatus("3");
                        }
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}

@ApiOperation(value = "postDeleteComments", nickname = "post Delete Comments")
@RequestMapping(method = RequestMethod.POST, path = "comments/delete", consumes = "application/json", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success"),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public Response postDeleteComments(@RequestBody CommentGet comment){
        Response response = new Response();
        DbHandler db = new DbHandler();

                if(db.getResult() == 0) {
                try{
                        String sql = "DELETE FROM comment where cmt_id = '"+comment.getCmtId()+"';";
                        if (!db.execute(sql)) {
                                response.setStatus("0");
                        }else{
                                response.setStatus("3");
                        }
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}

}


