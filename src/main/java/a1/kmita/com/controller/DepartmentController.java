package a1.kmita.com.controller;

import java.util.List;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.IOException;
import java.io.FileNotFoundException;

import a1.kmita.com.model.Response;
import a1.kmita.com.model.ResponseGet;
import a1.kmita.com.model.get.Department;

@RestController
@RequestMapping("/")
public class DepartmentController {

@ApiOperation(value = "getDept", nickname = "get Department")
@RequestMapping(method = RequestMethod.GET, path = "department", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success", response = Department.class),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public ResponseGet<List<Department> > getDept(){
        ResponseGet<List<Department> > response = new ResponseGet();
        List<Department> list = new ArrayList();
        DbHandler db = new DbHandler();
        if(db.getResult() == 0) {
                try{
                        String sql = "SELECT * FROM department ORDER BY department.dpt_id ASC;";
                        ResultSet rs = db.executeQuery(sql);
                        while(rs.next()) {
                                Department dpt = new Department();
                                dpt.setDptId(rs.getInt("dpt_id"));
                                dpt.setDptName(rs.getString("dpt_name"));
                                list.add(dpt);
                        }
                        if(list.size()>0) {
                                response.setData(list);
                                response.setStatus("0");
                        }else{
                                response.setStatus("3");
                        }
                        rs.close();
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        System.out.println(e.getMessage());
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}

//==================== Get Dpt By Id ===================//
@ApiOperation(value = "DptById", nickname = "get Department By Id")
@RequestMapping(method = RequestMethod.GET, path = "department/{dpt_id}", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success", response = Department.class),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public Response DptById(@PathVariable int dpt_id){
        ResponseGet<Department> response = new ResponseGet();
        Department dpt = new Department();
        DbHandler db = new DbHandler();
        if(db.getResult() == 0) {
                try{
                        //ResultSet rs = db.executeQuery("SELECT promo_id, promo_image, promo_link, promo_dsc, promo_create_time, promo_title, promo_end_time, promo_type FROM promo where promo_id = '" + promo_id + "';");
                        ResultSet rs = db.executeQuery("SELECT * FROM department where dpt_id = '" + dpt_id + "';");
                        if(rs.next()) {
                                dpt.setDptId(rs.getInt("dpt_id"));
                                dpt.setDptName(rs.getString("dpt_name"));
                        }
                        response.setSize(rs.getRow());
                        response.setStatus("0");
                        response.setData(dpt);
                        rs.close();
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        System.out.println(e.getMessage());
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}

}
