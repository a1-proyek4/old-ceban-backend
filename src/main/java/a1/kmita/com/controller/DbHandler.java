package a1.kmita.com.controller;

import java.sql.*;
import a1.kmita.com.controller.Auth;

public class DbHandler{

  public static int SQL_EXCEPTION = 1;
  public static int UNDEFINED_EXCEPTION = -2;
  public static int CLASS_NOT_FOUND_EXCEPTION = 2;
  public static int FAILED = 3;
  public static int FILE_NOT_FOUND_EXCEPTION = 4;
  public static int IOEXCEPTION = 5;
  public static int NOT_CONNECTED = -1;
  public static int SUCCEED = 0;

  private Connection connection;
  private Statement statement;

  int result = -1;

  public static String getMessage(int code){
    String message = "";
    switch(code){
      case -1 :
      message = "Tidak tersambung";
      break;
      case 0 :
      message =  "Sukses";
      break;
      case 1 :
      message =  "Bermasalah di syntax SQL";
      break;
      case 2 :
      message =  "Driver tidak ditemukan";
      break;
      case 3 :
      message = "Gagal";
      break;
      case 4 :
      message = "Gambar yang diunggah tidak ditemukan";
      break;
      case 5 :
      message = "I/O gagal";
      break;
      default :
      message =  "Terjadi masalah yang tidak terdefinisikan";
      break;
    }
    return message;
  }

  public Statement getStatement(){
    return statement;
  }

  public Connection getConnection(){
    return connection;
  }

  public DbHandler(){
      String url = Auth.POSTGRES_URL;
      String username = Auth.POSTGRES_USERNAME;
      String password =  Auth.POSTGRES_PASSWORD;
      connect(url, username, password);
  }

  public DbHandler(String url, String username, String password){
    connect(url, username, password);
  }

  public void connect(String url, String username, String password){
    // System.out.println(url+" "+username+" "+password);
    try {
        Class.forName("org.postgresql.Driver");
        try{
          connection = DriverManager.getConnection(url, username, password);
          statement = connection.createStatement();
          result = 0;
          //exceptionListener.onSuccess();
        }catch (SQLException e) {
          result = 1;
          System.out.println(e.getMessage());
            //exceptionListener.onError(1);
        }
    } catch (java.lang.ClassNotFoundException e) {
      result = 2;
      System.out.println(e.getMessage());
      //exceptionListener.onError(2);
    }
  }

  public int getResult(){
    return result;
  }

  public boolean execute(String sql) throws SQLException{
    System.out.println("execute : " + sql);
    return statement.execute(sql);
  }

  public ResultSet executeQuery(String sql) throws SQLException{
      System.out.println("executeQuery : " + sql);
      return statement.executeQuery(sql);
  }

    public int executeUpdate(String sql) throws SQLException{
        System.out.println("executeUpdate : " + sql);
        return statement.executeUpdate(sql);
    }

  public void closeStatement() throws SQLException{
    statement.close();
  }

  public void closeConnection() throws SQLException{
    connection.close();
  }
}
