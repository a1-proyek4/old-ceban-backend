package a1.kmita.com.controller;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.IOException;
import java.io.FileNotFoundException;


import a1.kmita.com.model.Response;
import a1.kmita.com.model.get.Gold;
import a1.kmita.com.model.get.Adv;
import a1.kmita.com.model.get.AdvDetail;
import a1.kmita.com.model.get.AdvImage;
import a1.kmita.com.model.ResponseGet;

import a1.kmita.com.controller.ImageHandler;

@RestController
@RequestMapping("/")
public class GoldController {

@ApiOperation(value = "getGolds", nickname = "get Golds")
@RequestMapping(method = RequestMethod.GET, path = "golds", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success", response = Gold.class),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public ResponseGet<List<Gold> > getGolds(        
        @RequestParam(name="q", defaultValue="", required = false) String query,
        @RequestParam(name="f", defaultValue="advert.ad_id", required = false) String field,
        @RequestParam(name="s", defaultValue="DESC", required = false) String sort,
        @RequestParam(name="p", defaultValue="1", required = false) int page,
        @RequestParam(name="l", defaultValue="10", required = false) int limit,
        @RequestParam(name="t", defaultValue="new", required = false) String type,
        @RequestParam(name="v", defaultValue="true", required = false) boolean verified){
        ResponseGet<List<Gold> > response = new ResponseGet();
        List<Gold> list = new ArrayList();

        int i = (limit * (page-1));
        int n = (limit * page);
        DbHandler db = new DbHandler();
        if(db.getResult() == 0) {
                try{
                        String sql = "SELECT gold.gold_id, gold.gold_create_time, gold.gold_end_time, advert.ad_id, advert.ad_title, advert.ad_price, (SELECT image.image_img FROM image WHERE image.ad_id = advert.ad_id LIMIT 1), advert.slr_nim, seller.slr_name, seller.slr_department FROM advert INNER JOIN seller ON advert.slr_nim = seller.slr_nim INNER JOIN gold ON advert.ad_id = gold.ad_id ORDER BY "+ field +" "+ sort +" LIMIT "+limit+" OFFSET "+i+";";
                        ResultSet rs = db.executeQuery(sql);
                        while(rs.next()) {
                                Gold gold = new Gold();
                                gold.setGoldId(rs.getInt("gold_id"));
                                gold.setAdId(rs.getInt("ad_id"));
                                gold.setGoldCreateTime(rs.getString("gold_create_time"));
                                gold.setGoldEndTime(rs.getString("gold_end_time"));
                                gold.setAdId(rs.getInt("ad_id"));
                                gold.setAdTitle(rs.getString("ad_title"));
                                gold.setAdPrice(rs.getInt("ad_price"));
                                gold.setAdImage(rs.getString("image_img"));
                                gold.setSlrNim(rs.getInt("slr_nim"));
                                gold.setSlrName(rs.getString("slr_name"));
                                gold.setSlrDepartment(rs.getString("slr_department"));
                                list.add(gold);
                        }
                        response.setSize(rs.getRow());
                        response.setData(list);
                        response.setStatus("0");
                        rs.close();
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        System.out.println(e.getMessage());
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}

@ApiOperation(value = "getGoldsById", nickname = "get Golds by Id")
@RequestMapping(method = RequestMethod.GET, path = "golds/{gold_id}", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success", response = Gold.class),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public ResponseGet<Gold> getGoldsById(@PathVariable int gold_id){
        ResponseGet<Gold> response = new ResponseGet();
        DbHandler db = new DbHandler();
        if(db.getResult() == 0) {
                try{
                        String sql = "SELECT gold.gold_id, gold.gold_create_time, gold.gold_end_time, advert.ad_id, advert.ad_title, advert.ad_price, (SELECT image.image_img FROM image WHERE image.ad_id = advert.ad_id LIMIT 1), advert.slr_nim, seller.slr_name, seller.slr_department FROM advert INNER JOIN seller ON advert.slr_nim = seller.slr_nim INNER JOIN gold ON advert.ad_id = gold.ad_id WHERE gold_id = "+gold_id+";";
                        ResultSet rs = db.executeQuery(sql);
                        if(rs.next()) {
                                Gold gold = new Gold();
                                gold.setGoldId(rs.getInt("gold_id"));
                                gold.setGoldCreateTime(rs.getString("gold_create_time"));
                                gold.setGoldEndTime(rs.getString("gold_end_time"));
                                gold.setAdId(rs.getInt("ad_id"));
                                gold.setAdTitle(rs.getString("ad_title"));
                                gold.setAdPrice(rs.getInt("ad_price"));
                                gold.setAdImage(rs.getString("image_img"));
                                gold.setSlrNim(rs.getInt("slr_nim"));
                                gold.setSlrName(rs.getString("slr_name"));
                                gold.setSlrDepartment(rs.getString("slr_department"));
                                response.setData(gold);

                        }
                        response.setSize(rs.getRow());
                        rs.close();
                        response.setStatus("0");
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        System.out.println(e.getMessage());
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}


@ApiOperation(value = "postDeleteGold", nickname = "post Delete Gold")
@RequestMapping(method = RequestMethod.POST, path = "golds/delete", consumes = "application/json", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success"),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public Response postDeleteGold(@RequestBody Gold gold){
        Response response = new Response();
        DbHandler db = new DbHandler();

                if(db.getResult() == 0) {
                try{
                        String sql = "DELETE FROM gold where gold_id = '"+gold.getGoldId()+"';";
                        if (!db.execute(sql)) {
                                response.setStatus("0");
                        }else{
                                response.setStatus("3");
                        }
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}


@ApiOperation(value = "postGold", nickname = "post Gold")
@RequestMapping(method = RequestMethod.POST, path = "golds/new",
                consumes = "application/json", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success"),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public Response postSellerRegister(@RequestBody Gold gold){
        Response response = new Response();
        DbHandler db = new DbHandler();
        if(db.getResult() == 0) {
                try{    
                        Date datefifteenleft = new Date(System.currentTimeMillis() + (15 * 1000 * 60 * 60 * 24));
                        String fifteenleft = new SimpleDateFormat("yyyy-MM-dd hh:mm").format(datefifteenleft);
                        String sql = "INSERT INTO gold (ad_id, gold_create_time, gold_end_time) VALUES ('"+gold.getAdId()+"', NOW(), '"+fifteenleft+"' );";
                        if(db.executeUpdate(sql) > 0) {
                                response.setStatus("0");
                        }else{
                                response.setStatus("3");
                        }
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        System.out.println(e.getMessage());
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}

}
