package a1.kmita.com.controller;

import java.util.List;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.IOException;
import java.io.FileNotFoundException;

import a1.kmita.com.model.post.PromoPost;
import a1.kmita.com.model.Response;
import a1.kmita.com.model.get.Promo;
import a1.kmita.com.model.ResponseGet;

import a1.kmita.com.controller.ImageHandler;

@RestController
@RequestMapping("/")
public class PromoController {

@ApiOperation(value = "postPromo", nickname = "post Promo")
@RequestMapping(method = RequestMethod.POST, path = "promos/new",
                consumes = "application/json", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success", response = Promo.class),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public Response postPromo(@RequestBody PromoPost promoPost){
        Response response = new Response();
        DbHandler db = new DbHandler();
        if(db.getResult() == 0) {
                try{
                        String sql = "INSERT INTO promo (promo.promo_image, promo.promo_link, promo.promo_dsc, promo.promo_create_time, promo.promo_title, promo.promo_end_time, promo.promo_type) VALUES ('"+ promoPost.getPromoImage() +"', '"+ promoPost.getPromoLink()+"', '"+ promoPost.getPromoDsc() +"', NOW(), '"+ promoPost.getPromoTitle() +"', '"+ promoPost.getPromoEndTime() +"','"+ promoPost.getPromoType() +"');";
                        if(db.executeUpdate(sql)>0) {
                                response.setStatus("0");
                        }
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        System.out.println(e.getMessage());
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}

@ApiOperation(value = "getPromos", nickname = "get Promos")
@RequestMapping(method = RequestMethod.GET, path = "promos", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success", response = Promo.class),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public ResponseGet<List<Promo> > getPromos(
        @RequestParam(name="q", defaultValue="", required = false) String query,
        @RequestParam(name="f", defaultValue="promo.promo_id", required = false) String field,
        @RequestParam(name="s", defaultValue="ASC", required = false) String sort,
        @RequestParam(name="p", defaultValue="1", required = false) int page,
        @RequestParam(name="l", defaultValue="5", required = false) int limit){
        ResponseGet<List<Promo> > response = new ResponseGet();
        List<Promo> list = new ArrayList();

        int i = (limit * (page-1));
        int n = (limit * page);

        DbHandler db = new DbHandler();
        if(db.getResult() == 0) {
                try{
                        String sql = "SELECT * FROM promo WHERE (promo.promo_dsc like '%" + query + "%' OR promo.promo_title like '%" + query + "%') ORDER BY "+ field +" "+ sort +", promo.promo_id ASC, promo.promo_end_time DESC LIMIT "+limit+" OFFSET "+i+";";
                        ResultSet rs = db.executeQuery(sql);
                        while(rs.next()) {
                                Promo promo = new Promo();
                                promo.setPromoId(rs.getInt("promo_id"));
                                promo.setPromoImage(rs.getString("promo_image"));
                                promo.setPromoLink(rs.getString("promo_link"));
                                promo.setPromoDsc(rs.getString("promo_dsc"));
                                promo.setPromoCreateTime(rs.getString("promo_create_time"));
                                promo.setPromoTitle(rs.getString("promo_title"));
                                promo.setPromoEndTime(rs.getString("promo_end_time"));
                                promo.setPromoType(rs.getString("promo_type"));
                                list.add(promo);
                        }
                        response.setSize(rs.getRow());
                        response.setData(list);
                        response.setStatus("0");
                        rs.close();
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        System.out.println(e.getMessage());
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}

@ApiOperation(value = "postPromoUpdateImage", nickname = "post Promo Update Image")
@RequestMapping(method = RequestMethod.POST, path = "promos/update_image",
                consumes = "multipart/form-data", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success"),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public Response postPromoUpdateImage(
        @RequestPart(name = "promoId", required = true) String promoId,
        @RequestPart(name = "slrImage", required = true) MultipartFile slrImage){
        Response response = new Response();
        DbHandler db = new DbHandler();
        ImageHandler imageHandler = null;
        if(slrImage != null) {
                imageHandler = new ImageHandler();
        }
        if(db.getResult() == 0) {
                try{
                        String uploadedUrl = "";
                        if(imageHandler != null) {
                                uploadedUrl = imageHandler.upload(slrImage);
                        }
                        String sql = "UPDATE promo SET promo.promo_image = '"+ uploadedUrl +"' WHERE promo.promo_id = '" + promoId + "';";
                        if(db.executeUpdate(sql) > 0) {
                                response.setStatus("0");
                        }else{
                                response.setStatus("3");
                        }
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        System.out.println(e.getMessage());
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }catch(FileNotFoundException e) {
                        response.setStatus("4");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }catch(IOException e) {
                        response.setStatus("5");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;

}


//==================== Get Promo By Id ===================//
@ApiOperation(value = "PromoById", nickname = "get Promo By Id")
@RequestMapping(method = RequestMethod.GET, path = "promos/{promo_id}", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success", response = Promo.class),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public Response PromoById(@PathVariable int promo_id){
        ResponseGet<Promo> response = new ResponseGet();
        Promo promo = new Promo();
        DbHandler db = new DbHandler();
        if(db.getResult() == 0) {
                try{
                        //ResultSet rs = db.executeQuery("SELECT promo_id, promo_image, promo_link, promo_dsc, promo_create_time, promo_title, promo_end_time, promo_type FROM promo where promo_id = '" + promo_id + "';");
                        ResultSet rs = db.executeQuery("SELECT *FROM promo where promo_id = '" + promo_id + "';");
                        if(rs.next()) {
                                promo.setPromoId(rs.getInt("promo_id"));
                                promo.setPromoImage(rs.getString("promo_image"));
                                promo.setPromoLink(rs.getString("promo_link"));
                                promo.setPromoDsc(rs.getString("promo_dsc"));
                                promo.setPromoCreateTime(rs.getString("promo_create_time"));
                                promo.setPromoTitle(rs.getString("promo_title"));
                                promo.setPromoEndTime(rs.getString("promo_end_time"));
                                promo.setPromoType(rs.getString("promo_type"));
                        }
                        response.setSize(rs.getRow());
                        response.setStatus("0");
                        response.setData(promo);
                        rs.close();
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        System.out.println(e.getMessage());
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}


//=============== Delete Promo ================
@ApiOperation(value = "postDeletePromo", nickname = "post Delete Promo")
@RequestMapping(method = RequestMethod.POST, path = "promos/delete", consumes = "application/json", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success"),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public Response postDeletePromo(@RequestParam int promo_id){
        Response response = new Response();
        DbHandler db = new DbHandler();

                if(db.getResult() == 0) {
                try{
                        String sql = "DELETE FROM promo where promo_id = '"+promo_id+"';";
                        if (!db.execute(sql)) {
                                response.setStatus("0");
                        }else{
                                response.setStatus("3");
                        }
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}

//===================== Promo By Slider ====================
@ApiOperation(value = "getPromoBySlider", nickname = "get Promos By Slider")
@RequestMapping(method = RequestMethod.GET, path = "promos/slider", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success", response = Promo.class),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public ResponseGet<List<Promo> > getPromoBySlider(
        @RequestParam(name="q", defaultValue="", required = false) String query,
        @RequestParam(name="f", defaultValue="promo.promo_id", required = false) String field,
        @RequestParam(name="s", defaultValue="ASC", required = false) String sort,
        @RequestParam(name="p", defaultValue="1", required = false) int page,
        @RequestParam(name="l", defaultValue="5", required = false) int limit){
        ResponseGet<List<Promo> > response = new ResponseGet();
        List<Promo> list = new ArrayList();

        int i = (limit * (page-1));
        int n = (limit * page);

        DbHandler db = new DbHandler();
        if(db.getResult() == 0) {
                try{
                        //String sql = "SELECT * FROM promo WHERE (promo.promo_dsc like '%" + query + "%' OR promo.promo_title like '%" + query + "%') ORDER BY "+ field +" "+ sort +", promo.promo_id ASC, promo.promo_end_time DESC LIMIT "+limit+" OFFSET "+i+";";
                        String sql="SELECT * FROM promo where promo_type='slider'";
                        ResultSet rs = db.executeQuery(sql);
                        while(rs.next()) {
                                Promo promo = new Promo();
                                promo.setPromoId(rs.getInt("promo_id"));
                                promo.setPromoImage(rs.getString("promo_image"));
                                promo.setPromoLink(rs.getString("promo_link"));
                                promo.setPromoDsc(rs.getString("promo_dsc"));
                                promo.setPromoCreateTime(rs.getString("promo_create_time"));
                                promo.setPromoTitle(rs.getString("promo_title"));
                                promo.setPromoEndTime(rs.getString("promo_end_time"));
                                promo.setPromoType(rs.getString("promo_type"));
                                list.add(promo);
                        }
                        response.setSize(rs.getRow());
                        response.setData(list);
                        response.setStatus("0");
                        rs.close();
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        System.out.println(e.getMessage());
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}
//===================== Promo By Slider ====================
@ApiOperation(value = "getPromoByBanner", nickname = "get Promos By Banner")
@RequestMapping(method = RequestMethod.GET, path = "promos/banner", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success", response = Promo.class),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public ResponseGet<List<Promo> > getPromoByBanner(
        @RequestParam(name="q", defaultValue="", required = false) String query,
        @RequestParam(name="f", defaultValue="promo.promo_id", required = false) String field,
        @RequestParam(name="s", defaultValue="ASC", required = false) String sort,
        @RequestParam(name="p", defaultValue="1", required = false) int page,
        @RequestParam(name="l", defaultValue="5", required = false) int limit){
        ResponseGet<List<Promo> > response = new ResponseGet();
        List<Promo> list = new ArrayList();

        int i = (limit * (page-1));
        int n = (limit * page);

        DbHandler db = new DbHandler();
        if(db.getResult() == 0) {
                try{
                        //String sql = "SELECT * FROM promo WHERE (promo.promo_dsc like '%" + query + "%' OR promo.promo_title like '%" + query + "%') ORDER BY "+ field +" "+ sort +", promo.promo_id ASC, promo.promo_end_time DESC LIMIT "+limit+" OFFSET "+i+";";
                        String sql="SELECT * FROM promo where promo_type='banner'";
                        ResultSet rs = db.executeQuery(sql);
                        while(rs.next()) {
                                Promo promo = new Promo();
                                promo.setPromoId(rs.getInt("promo_id"));
                                promo.setPromoImage(rs.getString("promo_image"));
                                promo.setPromoLink(rs.getString("promo_link"));
                                promo.setPromoDsc(rs.getString("promo_dsc"));
                                promo.setPromoCreateTime(rs.getString("promo_create_time"));
                                promo.setPromoTitle(rs.getString("promo_title"));
                                promo.setPromoEndTime(rs.getString("promo_end_time"));
                                promo.setPromoType(rs.getString("promo_type"));
                                list.add(promo);
                        }
                        response.setSize(rs.getRow());
                        response.setData(list);
                        response.setStatus("0");
                        rs.close();
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        System.out.println(e.getMessage());
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}


}
