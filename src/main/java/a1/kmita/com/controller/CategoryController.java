package a1.kmita.com.controller;

import java.util.List;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.sql.SQLException;
import java.sql.ResultSet;
import java.io.IOException;
import java.io.FileNotFoundException;

import a1.kmita.com.model.get.Category;
import a1.kmita.com.model.get.CategoryDetail;
import a1.kmita.com.model.get.Adv;
import a1.kmita.com.model.Response;
import a1.kmita.com.model.ResponseGet;
import a1.kmita.com.controller.ImageHandler;

@RestController
@RequestMapping("/")
public class CategoryController {

@ApiOperation(value = "getCategoryById", nickname = "get Category by Id")
@RequestMapping(method = RequestMethod.GET, path = "categories/{ctr_id}", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success", response = CategoryDetail.class),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public ResponseGet<CategoryDetail> getCategoryById(@PathVariable int ctr_id,
                                                   @RequestParam(name="w", defaultValue="false", required = false) boolean with_sub_categories){
        ResponseGet<CategoryDetail> response = new ResponseGet();
        CategoryDetail ctr = null;
        DbHandler db = new DbHandler();
        if(db.getResult() == 0) {
                try{
                        ResultSet rs = db.executeQuery("SELECT ctr_id, cat_ctr_id, ctr_name, ctr_image, ctr_dsc, ctr_ad_count FROM category where cat_ctr_id IS NULL AND ctr_id = '"+ ctr_id +"' LIMIT 1;");
                        if(rs.next()) {
                                ctr = new CategoryDetail();
                                ctr.setCtrId(rs.getInt("ctr_id"));
                                ctr.setCtrName(rs.getString("ctr_name"));
                                ctr.setCtrImage(rs.getString("ctr_image"));
                                ctr.setCtrDesc(rs.getString("ctr_dsc"));
                                ctr.setCtrAdCount(rs.getInt("ctr_ad_count"));
                        }
                        response.setSize(rs.getRow());
                        response.setStatus("0");
                        rs.close();
                        if(with_sub_categories && ctr != null) {
                                rs = db.executeQuery("SELECT ctr_id, cat_ctr_id, ctr_name, ctr_image, ctr_dsc, ctr_ad_count FROM category where cat_ctr_id = '" + ctr_id + "';");
                                while(rs.next()) {
                                        Category c = new Category();
                                        c.setCtrId(rs.getInt("ctr_id"));
                                        c.setCtrName(rs.getString("ctr_name"));
                                        c.setCtrImage(rs.getString("ctr_image"));
                                        c.setCtrDesc(rs.getString("ctr_dsc"));
                                        c.setCtrAdCount(rs.getInt("ctr_ad_count"));
                                        ctr.addSubCategories(c);
                                }
                                rs.close();
                        }
                        response.setData(ctr);
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        System.out.println(e.getMessage());
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}

@ApiOperation(value = "getSubCategoriesByCategoryId", nickname = "get SubCategories By Category Id")
@RequestMapping(method = RequestMethod.GET, path = "categories/{ctr_id}/sub_categories", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success", response = Category.class),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public ResponseGet<List<Category> > getSubCategoriesByCategoryId(@PathVariable int ctr_id){
        ResponseGet<List<Category> > response = new ResponseGet();
        List<Category> list = new ArrayList();
        DbHandler db = new DbHandler();
        if(db.getResult() == 0) {
                try{
                        ResultSet rs = db.executeQuery("SELECT ctr_id, cat_ctr_id, ctr_name, ctr_image, ctr_dsc, ctr_ad_count FROM category where cat_ctr_id = '" + ctr_id + "';");
                        while(rs.next()) {
                                Category c = new Category();
                                c.setCtrId(rs.getInt("ctr_id"));
                                c.setCtrName(rs.getString("ctr_name"));
                                c.setCtrImage(rs.getString("ctr_image"));
                                c.setCtrDesc(rs.getString("ctr_dsc"));
                                c.setCtrAdCount(rs.getInt("ctr_ad_count"));
                                list.add(c);
                        }
                        response.setSize(rs.getRow());
                        response.setStatus("0");
                        rs.close();
                        response.setData(list);
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        System.out.println(e.getMessage());
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}

@ApiOperation(value = "getCategories", nickname = "get Categories")
@RequestMapping(method = RequestMethod.GET, path = "categories", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success", response = CategoryDetail.class),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public ResponseGet<List<CategoryDetail> > getCategories(
        @RequestParam(name="w", defaultValue="false", required = false) boolean with_sub_categories,
        @RequestParam(name="q", defaultValue="", required = false) String query,
        @RequestParam(name="f", defaultValue="category.ctr_name", required = false) String field,
        @RequestParam(name="s", defaultValue="ASC", required = false) String sort,
        @RequestParam(name="p", defaultValue="1", required = false) int page,
        @RequestParam(name="l", defaultValue="10", required = false) int limit){
        ResponseGet<List<CategoryDetail> > response = new ResponseGet();
        List<CategoryDetail> list = new ArrayList();

        int i = (limit * (page-1));
        int n = (limit * page);

        DbHandler db = new DbHandler();
        if(db.getResult() == 0) {
                try{
                        ResultSet rs = db.executeQuery("SELECT ctr_id, cat_ctr_id, ctr_name, ctr_image, ctr_dsc, ctr_ad_count FROM category where cat_ctr_id IS NULL ORDER BY "+ field +" "+ sort +", category.ctr_id ASC LIMIT "+limit+" OFFSET "+i+";");
                        while(rs.next()) {
                                CategoryDetail ctr = new CategoryDetail();
                                ctr.setCtrId(rs.getInt("ctr_id"));
                                ctr.setCtrName(rs.getString("ctr_name"));
                                ctr.setCtrImage(rs.getString("ctr_image"));
                                ctr.setCtrDesc(rs.getString("ctr_dsc"));
                                ctr.setCtrAdCount(rs.getInt("ctr_ad_count"));
                                list.add(ctr);
                        }
                        response.setSize(rs.getRow());
                        response.setStatus("0");
                        rs.close();
                        if(with_sub_categories) {
                                for(int j = 0; j < list.size(); j++) {
                                        rs = db.executeQuery("SELECT ctr_id, cat_ctr_id, ctr_name, ctr_image, ctr_dsc, ctr_ad_count FROM category where cat_ctr_id = '" + list.get(j).getCtrId() + "';");
                                        while(rs.next()) {
                                                Category c = new Category();
                                                c.setCtrId(rs.getInt("ctr_id"));
                                                c.setCtrName(rs.getString("ctr_name"));
                                                c.setCtrImage(rs.getString("ctr_image"));
                                                c.setCtrDesc(rs.getString("ctr_dsc"));
                                                c.setCtrAdCount(rs.getInt("ctr_ad_count"));
                                                list.get(j).addSubCategories(c);
                                        }
                                        rs.close();
                                }
                        }
                        response.setData(list);
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        System.out.println(e.getMessage());
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}

@ApiOperation( value = "postNewCategory", nickname = "post New Category")
@RequestMapping( method = RequestMethod.POST, path = "categories/new",
                 consumes = "multipart/form-data", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success"),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public Response postNewCategory(@RequestPart(name = "catCtrId", required = false) String catCtrId,
                                @RequestPart(name = "ctrName", required = true) String ctrName,
                                @RequestPart(name = "ctrDesc", required = true) String ctrDesc,
                                @RequestPart(name = "ctrImage", required = false) MultipartFile ctrImage){
        Response response = new Response();
        ImageHandler imageHandler = null;
        if(ctrImage != null) {
                imageHandler = new ImageHandler();
        }
        DbHandler db = new DbHandler();

        if(db.getResult() == 0) {
                try{
                        String uploadedUrl = "";
                        if(imageHandler != null) {
                                uploadedUrl = imageHandler.upload(ctrImage);
                        }
                        String sql = "INSERT INTO category VALUES (cat_ctr_id, ctr_name, ctr_image, ctr_dsc, ctr_ad_count) VALUES (" + (catCtrId == null ? "NULL" : ("'"+ catCtrId +"'")) + ", '"+ ctrName +"', '"+ uploadedUrl +"', '"+ ctrDesc +"', 0);";
                        if(!db.execute(sql)) {
                                response.setStatus("0");
                        }else{
                                response.setStatus("3");
                        }
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }catch(FileNotFoundException e) {
                        response.setStatus("4");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }catch(IOException e) {
                        response.setStatus("5");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}

@ApiOperation(value = "postUpdateAdCountCategory", nickname = "post Update AdCount Category")
@RequestMapping(method = RequestMethod.POST, path = "categories/update_ad_count", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success"),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public Response postUpdateAdCountCategory(){
        Response response = new Response();
        DbHandler db = new DbHandler();

        List<String> list = new ArrayList();

        if(db.getResult() == 0) {
                try{
                        String sql = "SELECT ctr_id FROM category";
                        ResultSet rs = db.executeQuery(sql);
                        while(rs.next()) {
                                list.add(rs.getString("ctr_id"));
                        }
                        rs.close();
                        for(int i = 0; i < list.size(); i++) {
                                sql = "SELECT COUNT(advert_category.ad_id) FROM advert_category WHERE advert_category.ctr_id = '"+ list.get(i) +"'";
                                rs = db.executeQuery(sql);
                                if(rs.next()) {
                                        sql = "UPDATE category SET ctr_ad_count = '"+ rs.getString("count") +"';";
                                        db.execute(sql);
                                }
                                rs.close();
                        }
                        response.setStatus("0");
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}

@ApiOperation(value = "getAdsByCategory", nickname = "get Ads by Category")
@RequestMapping(method = RequestMethod.GET, path = "categories/{ctr_id}/ads", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success", response = Adv.class),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public ResponseGet<List<Adv> > getAdsbyCategory(
        @PathVariable int ctr_id,
        @RequestParam(name="q", defaultValue="", required = false) String query,
        @RequestParam(name="f", defaultValue="advert.ad_id", required = false) String field,
        @RequestParam(name="s", defaultValue="DESC", required = false) String sort,
        @RequestParam(name="p", defaultValue="1", required = false) int page,
        @RequestParam(name="l", defaultValue="10", required = false) int limit,
        @RequestParam(name="v", defaultValue="false", required = false) boolean verified){
        ResponseGet<List<Adv> > response = new ResponseGet();
        List<Adv> list = new ArrayList();

        int i = (limit * (page-1));
        int n = (limit * page);

        DbHandler db = new DbHandler();
        if(db.getResult() == 0) {
                try{
                        String sql = "SELECT advert.ad_id, advert.ad_title, advert.ad_price, (SELECT image.image_img FROM image WHERE image.ad_id = advert.ad_id LIMIT 1), advert.slr_nim, seller.slr_name, seller.slr_department FROM advert INNER JOIN seller ON advert.slr_nim = seller.slr_nim INNER JOIN advert_category ON advert.ad_id = advert_category.ad_id INNER JOIN category ON advert_category.ctr_id = category.ctr_id WHERE advert_category.ctr_id = '" + ctr_id + "' AND (advert.ad_title LIKE '%"+ query +"%' OR advert.ad_desc LIKE '%"+ query +"%') AND ad_stat = "+ verified +" ORDER BY "+ field +" "+ sort +", advert.ad_bump DESC LIMIT "+limit+" OFFSET "+i+";";
                        ResultSet rs = db.executeQuery(sql);
                        while(rs.next()) {
                                Adv ad = new Adv();
                                ad.setAdId(rs.getInt("ad_id"));
                                ad.setAdTitle(rs.getString("ad_title"));
                                ad.setAdPrice(rs.getInt("ad_price"));
                                ad.setAdImage(rs.getString("image_img"));
                                ad.setSlrNim(rs.getInt("slr_nim"));
                                ad.setSlrName(rs.getString("slr_name"));
                                ad.setSlrDepartment(rs.getString("slr_department"));
                                list.add(ad);
                        }
                        response.setSize(rs.getRow());
                        response.setStatus("0");
                        response.setData(list);
                        rs.close();
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        System.out.println(e.getMessage());
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}


@ApiOperation(value = "postEditCategory", nickname = "Post Edit Category")
@RequestMapping(method = RequestMethod.POST, path = "categories/edit",
                consumes = "application/json", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success"),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public  Response postEditCategory(@RequestBody Category ctr){
        Response response = new Response();
        DbHandler db = new DbHandler();
        if(db.getResult() == 0) {
                try{
                        String sql = "UPDATE category SET ctr_name ='"+ctr.getCtrName()+"',ctr_image ='"+ctr.getCtrImage()+"', ctr_dsc ='"+ctr.getCtrDesc()+"', ctr_ad_count ='"+ctr.getCtrAdCount()+"' WHERE ctr_id="+ctr.getCtrId()+";";
                        if(!db.execute(sql)) {
                                response.setStatus("0");
                        }else{
                                response.setStatus("3");
                        }
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}

}
