package a1.kmita.com.controller;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.IOException;
import java.io.FileNotFoundException;

import a1.kmita.com.model.get.Adv;
import a1.kmita.com.model.get.AdvDetail;
import a1.kmita.com.model.get.AdvImage;
import a1.kmita.com.model.post.AdPost;
import a1.kmita.com.model.get.CommentGet;
import a1.kmita.com.model.post.BumpPost;
import a1.kmita.com.model.Response;
import a1.kmita.com.model.ResponsePost;
import a1.kmita.com.listener.ExceptionListener;
import a1.kmita.com.model.ResponseGet;
import a1.kmita.com.controller.ImageHandler;
import a1.kmita.com.model.post.ReportPost;


@RestController
@RequestMapping("/")
public class AdvController {

@ApiOperation(value = "getAds", nickname = "get All Ads")
@RequestMapping(method = RequestMethod.GET, path = "ads", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success", response = Adv.class),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public ResponseGet<List<Adv> > getAllAds(
        @RequestParam(name="q", defaultValue="", required = false) String query,
        @RequestParam(name="f", defaultValue="advert.ad_id", required = false) String field,
        @RequestParam(name="s", defaultValue="DESC", required = false) String sort,
        @RequestParam(name="p", defaultValue="1", required = false) int page,
        @RequestParam(name="l", defaultValue="10", required = false) int limit,
        @RequestParam(name="t", defaultValue="new", required = false) String type,
        @RequestParam(name="v", defaultValue="true", required = false) boolean verified){
        ResponseGet<List<Adv>> response = new ResponseGet();
        List<Adv> list = new ArrayList();

        int i = (limit * (page-1));
        int n = (limit * page);

        DbHandler db = new DbHandler();
        if(db.getResult() == 0) {
                try{
                        String sql = "";
                        if(type.equals("hot")) {
                                // String sevendaysago = new SimpleDateFormat("yyyy-MM-dd hh:mm")
                                //                       .format(new Date(System.currentTimeMillis() - (7 * (1000 * 60 * 60 * 24))));
                                sql = "SELECT advert.ad_id, advert.ad_title, advert.ad_price, (SELECT image.image_img FROM image WHERE image.ad_id = advert.ad_id LIMIT 1), advert.slr_nim, seller.slr_name, seller.slr_department FROM advert INNER JOIN seller ON advert.slr_nim = seller.slr_nim WHERE (advert.ad_title like '%" + query + "%' OR advert.ad_desc like '%" + query + "%') AND ad_stat = "+ verified +" ORDER BY "+ field +" "+ sort +", advert.ad_bump, advert.ad_count DESC LIMIT "+limit+" OFFSET "+i+" ;";
                        }else if(type.equals("new")) {
                                sql = "SELECT advert.ad_id, advert.ad_title, advert.ad_price, (SELECT image.image_img FROM image WHERE image.ad_id = advert.ad_id LIMIT 1), advert.slr_nim, seller.slr_name, seller.slr_department FROM advert INNER JOIN seller ON advert.slr_nim = seller.slr_nim WHERE (advert.ad_title like '%" + query + "%' OR advert.ad_desc like '%" + query + "%') AND ad_stat = "+ verified +"  ORDER BY "+ field +" "+ sort +", advert.ad_bump DESC LIMIT "+limit+" OFFSET "+i+" ;";
                        }else if(type.equals("gold")) {
                                sql = "SELECT advert.ad_id, advert.ad_title, advert.ad_price, (SELECT image.image_img FROM image WHERE image.ad_id = advert.ad_id LIMIT 1), advert.slr_nim, seller.slr_name, seller.slr_department FROM gold INNER JOIN advert ON gold.ad_id = advert.ad_id INNER JOIN seller ON advert.slr_nim = seller.slr_nim WHERE (advert.ad_title like '%" + query + "%' OR advert.ad_desc like '%" + query + "%') AND ad_stat = "+ verified +" AND (DATE(NOW()) <= DATE(gold.gold_end_time)) ORDER BY "+ field +" "+ sort +", advert.ad_bump, advert.ad_count DESC LIMIT "+limit+" OFFSET "+i+";";
                        }
                        ResultSet rs = db.executeQuery(sql);
                        while(rs.next()) {
                                Adv ad = new Adv();
                                ad.setAdId(rs.getInt("ad_id"));
                                ad.setAdTitle(rs.getString("ad_title"));
                                ad.setAdPrice(rs.getInt("ad_price"));
                                ad.setAdImage(rs.getString("image_img"));
                                ad.setSlrNim(rs.getInt("slr_nim"));
                                ad.setSlrName(rs.getString("slr_name"));
                                ad.setSlrDepartment(rs.getString("slr_department"));
                                list.add(ad);
                        }
                        response.setSize(rs.getRow());
                        response.setStatus("0");
                        response.setData(list);
                        rs.close();
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        System.out.println(e.getMessage());
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}

@ApiOperation(value = "postAdBump", nickname = "post Ad Bump")
@RequestMapping(method = RequestMethod.POST, path = "ads/bump",
                consumes = "application/json", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success"),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public Response postAdBump(@RequestBody BumpPost bumpPost){
        Response response = new Response();
        DbHandler db = new DbHandler();
        if(db.getResult() == 0) {
                try{
                        Date datesevendaysago = new Date(System.currentTimeMillis() - (7 * 1000 * 60 * 60 * 24));
                        String sevendaysago = new SimpleDateFormat("yyyy-MM-dd hh:mm").format(datesevendaysago);
                        String sql = "SELECT advert.ad_bump FROM advert WHERE advert.ad_id = '" + bumpPost.getAdId() + "' AND advert.slr_nim = '" + bumpPost.getSlrNim() + "' AND DATE(advert.ad_bump) <= DATE('"+ sevendaysago  +"') LIMIT 1;";
                        ResultSet rs = db.executeQuery(sql);
                        response.setStatus("3");
                        if(rs.next()) {
                                sql = "UPDATE advert SET advert.ad_bump = NOW() WHERE advert.ad_id = '" + bumpPost.getAdId() + "' AND advert.slr_nim = '" + bumpPost.getSlrNim() + "';";
                                if(db.executeUpdate(sql) > 0) {
                                        response.setStatus("0");
                                }
                        }
                        rs.close();
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        System.out.println(e.getMessage());
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}

@ApiOperation(value = "postAd", nickname = "post Ad")
@RequestMapping(method = RequestMethod.POST, path = "ads/new",
                consumes = "application/json", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success", response = ResponsePost.class),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public ResponsePost postAd(@RequestBody AdPost adPost){
        ResponsePost response = new ResponsePost();
        DbHandler db = new DbHandler();
        if(db.getResult() == 0) {
                try{
                        Date datemonthleft = new Date(System.currentTimeMillis() + (10 * 1000 * 60 * 60 * 24));
                        String monthleft = new SimpleDateFormat("yyyy-MM-dd hh:mm").format(datemonthleft);
                        Date datetoday = new Date(System.currentTimeMillis());
                        String today = new SimpleDateFormat("yyyy-MM-dd hh:mm").format(datetoday);
                        String sql = "INSERT INTO advert (slr_nim, ad_title, ad_desc, ad_price, ad_meet_place, ad_contact, ad_bump, ad_create_date, ad_stat, ad_end_date) VALUES ('"+ adPost.getSlrNim() +"', '"+ adPost.getAdTitle() +"', '"+ adPost.getAdDesc() +"', '"+ adPost.getAdPrice() +"', '"+ adPost.getAdMeetPlace() +"', '"+ adPost.getAdContact() +"', NOW(), NOW(), false, '"+ monthleft +"') RETURNING ad_id;";
                        ResultSet rs = db.executeQuery(sql);
                        if(rs.next()) {
                                response.setId(rs.getInt("ad_id"));
                                sql = "INSERT INTO advert_category VALUES ('"+ adPost.getCtrId() +"', '"+ rs.getString("ad_id") +"'),((SELECT cat_ctr_id FROM category WHERE ctr_id = '"+ adPost.getCtrId() +"'), '"+ rs.getString("ad_id") +"');";
                                if(db.executeUpdate(sql) > 0) {
                                        response.setStatus("0");
                                }
                        }
                        rs.close();
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        System.out.println(e.getMessage());
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}



@ApiOperation(value = "getAdById", nickname = "get Ad Detail")
@RequestMapping(method = RequestMethod.GET, path = "ads/{ad_id}", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success", response = AdvDetail.class),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public ResponseGet<AdvDetail> getAdsById(@PathVariable int ad_id){
        ResponseGet<AdvDetail> response = new ResponseGet();
        DbHandler db = new DbHandler();
        AdvDetail addetail = null;
        if(db.getResult() == 0) {
                try{
                        String sql = "SELECT advert.ad_id, advert.ad_title, advert.ad_desc, advert.ad_price, (SELECT image.image_img FROM image WHERE image.ad_id = advert.ad_id LIMIT 1), advert.ad_meet_place, advert.ad_contact, advert.ad_bump, advert.ad_create_date, advert.ad_stat, advert.ad_end_date, advert.slr_nim, seller.slr_name, seller.slr_department, category.ctr_id, category.ctr_name FROM advert INNER JOIN seller on seller.slr_nim = advert.slr_nim INNER JOIN advert_category ON advert_category.ad_id = advert_category.ad_id INNER JOIN category ON advert_category.ctr_id = category.ctr_id WHERE advert.ad_id = '"+ ad_id +"' LIMIT 1;";
                        ResultSet rs = db.executeQuery(sql);
                        if(rs.next()) {
                                addetail = new AdvDetail();
                                addetail.setAdId(rs.getInt("ad_id"));
                                addetail.setAdTitle(rs.getString("ad_title"));
                                addetail.setAdDesc(rs.getString("ad_desc"));
                                addetail.setAdPrice(rs.getInt("ad_price"));
                                addetail.setAdMeetPlace(rs.getString("ad_meet_place"));
                                addetail.setAdContact(rs.getString("ad_contact"));
                                addetail.setAdBump(rs.getString("ad_bump"));
                                addetail.setAdCreateDate(rs.getString("ad_create_date"));
                                addetail.setAdStat(rs.getBoolean("ad_stat"));
                                addetail.setAdDeadDate(rs.getString("ad_end_date"));
                                addetail.setAdImage(rs.getString("image_img"));
                                addetail.setSlrNim(rs.getInt("slr_nim"));
                                addetail.setSlrName(rs.getString("slr_name"));
                                addetail.setSlrDepartment(rs.getString("slr_department"));
                                addetail.setCtrId(rs.getInt("ctr_id"));
                                addetail.setCtrName(rs.getString("ctr_name"));
                        }
                        response.setSize(rs.getRow());
                        rs.close();
                        if(addetail != null) {
                                sql = "SELECT image_img FROM image WHERE ad_id = '" + addetail.getAdId() + "' LIMIT 5;";
                                rs =  db.executeQuery(sql);
                                while(rs.next()) {
                                        addetail.addImage(new AdvImage(rs.getString("image_img")));
                                }
                                rs.close();
                                response.setData(addetail);
                        }
                        response.setStatus("0");
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        System.out.println(e.getMessage());
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}

@ApiOperation(value = "getAdsComment", nickname = "get Ads Comment")
@RequestMapping(method = RequestMethod.GET, path = "ads/{ad_id}/comments", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success", response = CommentGet.class),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public ResponseGet<List<CommentGet> > getAdsComment(
        @PathVariable int ad_id,
        @RequestParam(name="f", defaultValue="cmt_id", required = false) String field,
        @RequestParam(name="s", defaultValue="DESC", required = false) String sort,
        @RequestParam(name="p", defaultValue="1", required = false) int page,
        @RequestParam(name="l", defaultValue="20", required = false) int limit,
        @RequestParam(name="t", defaultValue="new", required = false) String type){
        ResponseGet<List<CommentGet> > response = new ResponseGet();
        List<CommentGet> list = new ArrayList();

        int i = (limit * (page-1));
        int n = (limit * page);

        DbHandler db = new DbHandler();
        if(db.getResult() == 0) {
                try{
                        String sql = "SELECT cmt_id, cmt_msg, cmt_email, cmt_date, cmt_stat FROM comment WHERE ad_id = "+ad_id+" ORDER BY "+ field +" "+ sort +" LIMIT "+limit+" OFFSET "+i+" ;";
                        ResultSet rs = db.executeQuery(sql);
                        while(rs.next()) {
                                CommentGet cmt = new CommentGet();
                                cmt.setCmtId(rs.getInt("cmt_id"));
                                cmt.setCmtMsg(rs.getString("cmt_msg"));
                                cmt.setCmtEmail(rs.getString("cmt_email"));
                                cmt.setCmtDate(rs.getString("cmt_date"));
                                cmt.setCmtStatus(rs.getBoolean("cmt_stat"));
                                list.add(cmt);
                        }
                        response.setSize(rs.getRow());
                        response.setStatus("0");
                        response.setData(list);
                        rs.close();
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        System.out.println(e.getMessage());
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}

@ApiOperation(value = "verifyAd", nickname = "verify Ad")
@RequestMapping(method = RequestMethod.POST, path = "ads/verify",
                consumes = "application/json", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success"),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public Response verifyAd(@RequestBody Adv adv){
        Response response = new Response();
        DbHandler db = new DbHandler();
        if(db.getResult() == 0) {
                try{
                        String sql = "UPDATE advert SET ad_stat = true WHERE ad_id = "+adv.getAdId()+";";
                        if(!db.execute(sql)) {
                                response.setStatus("0");
                        }else{
                                response.setStatus("3");
                        }
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}

@ApiOperation(value = "postEditAds", nickname = "post Edit Ads")
@RequestMapping(method = RequestMethod.POST, path = "ads/edit", produces = "application/json")
@ApiResponses(value = {
                //@ApiResponse(code = 200, message = "Success", response = AdvDetail.class),
                @ApiResponse(code = 200, message = "Success"),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public Response postEditAds(@RequestParam(name = "adId", required = true) int adId,
                            @RequestParam(name = "adTitle", required = false) String adTitle,
                            @RequestParam(name = "adDesc", required = false) String adDesc,
                            @RequestParam(name = "adPrice", required = false) String adPrice,
                            @RequestParam(name = "adMeetPlace", required = false) String adMeetPlace,
                            @RequestParam(name = "adContact", required = false) String adContact,
                            @RequestParam(name = "adBump", required = false) String adBump,
                            @RequestParam(name = "adCreateDate", required = false) String adCreateDate,
                            @RequestParam(name = "adEndDate", required = false) String adEndDate,
                            @RequestParam(name = "slrNim", required = true) String slrNim,
                            @RequestParam(name = "viewId", required = false) String viewId,
                            @RequestParam(name = "adCount", required = false) String adCount){
        Response response = new Response(); //harus import dulu ternyata, dari ResponseGet ke Response
        AdvDetail addetail = new AdvDetail();
        DbHandler db = new DbHandler();
        if(db.getResult() == 0) {
                try{
                        String uploadedUrl = "";
                        String sql = "UPDATE advert SET " + (adEndDate == null ? "" : ("ad_end_date = '"+adEndDate+"', ")) + (viewId == null ? "" : ("view_id = '"+viewId+"', ")) + (adTitle == null ? "" : ("ad_title = '"+adTitle+"', ")) + (adDesc == null ? "" : ("ad_desc = '"+adDesc+"', ")) + (adPrice == null ? "" : ("ad_price = '"+adPrice+"', ")) + (adMeetPlace == null ? "" : ("ad_meet_place = '"+adMeetPlace+"', ")) + (adContact == null ? "" : ("ad_contact = '"+adContact+"', ")) + (adBump == null ? "" : ("ad_bump = '"+adBump+"', ")) + (adCreateDate == null ? "" : ("ad_create_date = '"+adCreateDate+"', ")) + (adCount == null ? "" : ("ad_count = '"+adCount+"', ")) + (slrNim == null ? "" : ("slr_nim = '"+slrNim+"' ")) + " WHERE ad_id = '"+adId+"';";
                        //String sql = "UPDATE advert SET " + (adTitle == null ? "" : ("ad_title = '" + adTitle + "', ")) + (adDesc == null ? "" : ("ad_desc = '" + adDesc + "' ")) + " WHERE ad_id = '"+ adId +"';";

                        if (!db.execute(sql)) {
                                response.setStatus("0");
                                //response.setData(addetail);
                        }else{
                                response.setStatus("3");
                        }
                        //ResultSet rs = db.executeQuery("SELECT advert.ad_id, advert.ad_title, advert.ad_desc, advert.ad_price, advert.ad_meet_place, advert.ad_contact, advert.ad_bump, advert.ad_create_date, advert.ad_stat, advert.ad_end_date, advert.ad_image1, advert.ad_image2, advert.ad_image3, advert.ad_image4, advert.ad_image5 FROM advert where advert.ad_id = '"+ad_id+"';");
                        //if(rs.next()) {
                        //}
                        //response.setStatus("0");
                        //response.setData(addetail);
                        //rs.close();
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        //System.out.println(e.getMessage());
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }


                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}

@ApiOperation(value = "postAdsUpdateImage", nickname = "post Ads Update Image")
@RequestMapping(method = RequestMethod.POST, path = "ads/update_image",
                consumes = "multipart/form-data", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success"),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public Response postAdsUpdateImage(
        @RequestPart(name = "adId", required = true) String adId,
        @RequestPart(name = "adImage", required = true) MultipartFile adImage){
        Response response = new Response();
        DbHandler db = new DbHandler();
        ImageHandler imageHandler = null;
        if(adImage != null) {
                imageHandler = new ImageHandler();
        }
        if(db.getResult() == 0) {
                try{
                        String uploadedUrl = "";
                        if(imageHandler != null) {
                                uploadedUrl = imageHandler.upload(adImage);
                        }
                        String sql = "INSERT INTO image (ad_id, image_img) VALUES ('" + adId + "', '"+ uploadedUrl +"');";
                        if(db.executeUpdate(sql) > 0) {
                                response.setStatus("0");
                        }else{
                                response.setStatus("3");
                        }
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        System.out.println(e.getMessage());
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }catch(FileNotFoundException e) {
                        response.setStatus("4");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }catch(IOException e) {
                        response.setStatus("5");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;

}


@ApiOperation(value = "reportAd", nickname = "report Ad")
@RequestMapping(method = RequestMethod.POST, path = "ads/report",
                consumes = "application/json", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success"),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public Response reportAd(@RequestBody ReportPost report){
        Response response = new Response();
        DbHandler db = new DbHandler();
        if(db.getResult() == 0) {
                try{    
                        String sql = "INSERT INTO report (ad_id, rpt_name, rpt_email, rpt_cmt, rpt_cause, rpt_date) VALUES ('"+report.getAdId()+"', '"+report.getRptName()+"', '"+report.getRptEmail()+"', '"+report.getRptCmt()+"', '"+report.getRptCause()+"', NOW()) ;";
                        if(!db.execute(sql)) {
                                response.setStatus("0");
                        }else{
                                response.setStatus("3");
                        }
                        db.closeStatement();
                        db.closeConnection();
                }catch(SQLException e) {
                        response.setStatus("1");
                        try{
                                db.closeConnection();
                                db.closeStatement();
                        }catch(SQLException e1) {

                        }
                }
        }else{
                response.setStatus(String.valueOf(db.getResult()));
        }
        return response;
}


}



// @ApiOperation(value = "deactiveAd", nickname = "deactive Ad")
// @RequestMapping(method = RequestMethod.POST, path = "ads/deactive", produces = "application/json")
// @ApiResponses(value = {
//                 @ApiResponse(code = 200, message = "Success"),
//                 @ApiResponse(code = 401, message = "Unauthorized"),
//                 @ApiResponse(code = 403, message = "Forbidden"),
//                 @ApiResponse(code = 404, message = "Not Found"),
//                 @ApiResponse(code = 500, message = "Failure")
//         })
// public Response deactiveAd(@RequestParam(name = "adsId", required = true) int ad_id){
//         Response response = new Response();
//         DbHandler db = new DbHandler();
//         if(db.getResult() == 0) {
//                 try{
//                         String sql = "UPDATE advert SET ad_stat = false WHERE ad_id = "+ad_id+";";
//                         if(!db.execute(sql)) {
//                                 response.setStatus("0");
//                         }else{
//                                 response.setStatus("3");
//                         }
//                         db.closeStatement();
//                         db.closeConnection();
//                 }catch(SQLException e) {
//                         response.setStatus("1");
//                         try{
//                                 db.closeConnection();
//                                 db.closeStatement();
//                         }catch(SQLException e1) {
//
//                         }
//                 }
//         }else{
//                 response.setStatus(String.valueOf(db.getResult()));
//         }
//         return response;
// }
