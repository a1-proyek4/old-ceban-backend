package a1.kmita.com.model.post;

public class LoginPost {

private String slrId;
private String slrPass;

public LoginPost() {
}

public String getSlrId() {
        return slrId;
}

public void setSlrId(String slrId) {
        this.slrId = slrId;
}

public String getSlrPass() {
        return slrPass;
}

public void setSlrPass(String slrPass) {
        this.slrPass = slrPass;
}

}
