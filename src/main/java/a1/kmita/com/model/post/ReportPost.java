package a1.kmita.com.model.post;


public class ReportPost{

    private Integer ad_id;
    private String rpt_name;
    private String rpt_email;
    private String rpt_cmt;
    private String rpt_cause;
    

    public Integer getAdId() {
        return ad_id;
    }

    public void setAdId(Integer ad_id) {
        this.ad_id = ad_id;
    }

    public String getRptName(){
        return rpt_name;
    }

    public void setRptName(String rpt_name){
        this.rpt_name = rpt_name;
    }

    public String getRptEmail(){
        return rpt_email;
    }

    public void setRptEmail(String rpt_email){
        this.rpt_email = rpt_email;
    }

    public String getRptCmt(){
        return rpt_cmt;
    }

    public void setRptCmt(String rpt_cmt){
        this.rpt_cmt = rpt_cmt;
    }

    public String getRptCause(){
        return rpt_cause;
    }

    public void setRptCause(String rpt_cause){
        this.rpt_cause = rpt_cause;
    }
}