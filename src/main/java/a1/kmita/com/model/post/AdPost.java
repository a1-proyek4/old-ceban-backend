package a1.kmita.com.model.post;


public class AdPost {

private Integer slrNim;
private String adTitle;
private String adDesc;
private Integer adPrice;
private String adMeetPlace;
private String adContact;
private Integer ctrId;
private boolean adStatus;

public Integer getSlrNim() {
        return slrNim;
}

public void setSlrNim(Integer slrNim) {
        this.slrNim = slrNim;
}

public String getAdTitle() {
        return adTitle;
}

public void setAdTitle(String adTitle) {
        this.adTitle = adTitle;
}

public String getAdDesc() {
        return adDesc;
}

public void setAdDesc(String adDesc) {
        this.adDesc = adDesc;
}

public Integer getAdPrice() {
        return adPrice;
}

public void setAdPrice(Integer adPrice) {
        this.adPrice = adPrice;
}

public String getAdMeetPlace() {
        return adMeetPlace;
}

public void setAdMeetPlace(String adMeetPlace) {
        this.adMeetPlace = adMeetPlace;
}

public String getAdContact() {
        return adContact;
}

public void setAdContact(String adContact) {
        this.adContact = adContact;
}

public Integer getCtrId() {
        return ctrId;
}

public void setCtrId(Integer ctrId) {
        this.ctrId = ctrId;
}

public boolean getAdStatus(){
        return adStatus;
}

public void setAdStatus(){
        this.adStatus = adStatus;
}

}
