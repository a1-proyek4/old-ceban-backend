package a1.kmita.com.model.post;


public class PromoPost {

private String promo_image;
private String promo_link;
private String promo_dsc;
private String promo_title;
private String promo_end_time;
private String promo_type;

public PromoPost(){

}

public String getPromoImage(){
        return promo_image;
}

public String getPromoLink(){
        return promo_link;
}

public String getPromoDsc(){
        return promo_dsc;
}

public String getPromoTitle(){
        return promo_title;
}

public String getPromoEndTime(){
        return promo_end_time;
}

public String getPromoType(){
        return promo_type;
}

public void setPromoImage(String promo_image){
        this.promo_image = promo_image;
}

public void setPromoLink(String promo_link){
        this.promo_link = promo_link;
}


public void setPromoDsc(String promo_dsc){
        this.promo_dsc = promo_dsc;
}

public void setPromoTitle(String promo_title){
        this.promo_title = promo_title;
}

public void setPromoEndTime(String promo_end_time){
        this.promo_end_time = promo_end_time;
}

public void setPromoType(String promo_type){
        this.promo_type = promo_type;
}

}
