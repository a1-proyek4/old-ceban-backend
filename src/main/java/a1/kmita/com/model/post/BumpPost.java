package a1.kmita.com.model.post;

public class BumpPost {

private Integer adId;
private Integer slrNim;

public BumpPost() {
}

public Integer getAdId() {
        return adId;
}

public void setAdId(Integer adId) {
        this.adId = adId;
}

public Integer getSlrNim() {
        return slrNim;
}

public void setSlrNim(Integer slrNim) {
        this.slrNim = slrNim;
}

}
