package a1.kmita.com.model.get;


public class Report{

    private Integer ad_id;
    private Integer rpt_id;
    private String rpt_name;
    private String rpt_email;
    private String rpt_cmt;
    private String rpt_cause;
    private String rpt_date;
    private String ad_title;
    
    public Report(){
    }

    public Integer getAdId() {
        return ad_id;
    }

    public void setAdId(Integer ad_id) {
        this.ad_id = ad_id;
    }

    public Integer getRptId(){
        return rpt_id;
    }

    public void setRptId(Integer rpt_id){
        this.rpt_id = rpt_id;
    }

    public String getRptName(){
        return rpt_name;
    }

    public void setRptName(String rpt_name){
        this.rpt_name = rpt_name;
    }

    public String getRptEmail(){
        return rpt_email;
    }

    public void setRptEmail(String rpt_email){
        this.rpt_email = rpt_email;
    }

    public String getRptCmt(){
        return rpt_cmt;
    }

    public void setRptCmt(String rpt_cmt){
        this.rpt_cmt = rpt_cmt;
    }

    public String getRptCause(){
        return rpt_cause;
    }

    public void setRptCause(String rpt_cause){
        this.rpt_cause = rpt_cause;
    }

    public String getRptDate(){
        return rpt_date;
    }

    public void setRptDate(String rpt_date){
        this.rpt_date = rpt_date;
    }

    public String getAdTitle(){
        return ad_title;
    }

    public void setAdTitle(String ad_title){
        this.ad_title = ad_title;
    }
}