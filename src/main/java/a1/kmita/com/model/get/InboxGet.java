package a1.kmita.com.model.get;

public class InboxGet extends Inbox {

private int ibx_id;
private String ibx_sd_date;

public InboxGet(){

}

public int getIbxId(){
        return ibx_id;
}

public void setIbxId(int ibx_id){
  this.ibx_id = ibx_id;
}

public String getIbxSdDate(){
        return ibx_sd_date;
}

public void setIbxSdDate(String ibx_sd_date){
        this.ibx_sd_date = ibx_sd_date;
}

}
