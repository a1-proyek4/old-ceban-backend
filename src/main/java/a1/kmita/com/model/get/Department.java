package a1.kmita.com.model.get;

public class Department {

private int dpt_id;
private String dpt_name;

public Department() {
}

public int getDptId(){
        return dpt_id;
}

public String getDptName(){
        return dpt_name;
}

public void setDptId(int dpt_id){
        this.dpt_id = dpt_id;
}

public void setDptName(String dpt_name){
        this.dpt_name = dpt_name;
}

}
