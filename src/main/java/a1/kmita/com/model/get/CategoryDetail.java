package a1.kmita.com.model.get;

import java.util.ArrayList;
import java.util.List;

public class CategoryDetail extends Category {

  private List<Category> ctr_sub_categories;

  public CategoryDetail(){

  }

  public List<Category> getCtrSubCategories(){
    return ctr_sub_categories;
  }

  public void setCtrSubCategories(List<Category> ctr_sub_categories){
    this.ctr_sub_categories = ctr_sub_categories;
  }

  public void addSubCategories(Category sub_category){
    if(ctr_sub_categories == null){
      ctr_sub_categories = new ArrayList();
    }
    ctr_sub_categories.add(sub_category);
  }

}
