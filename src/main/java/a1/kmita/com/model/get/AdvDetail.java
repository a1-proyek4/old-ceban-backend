package a1.kmita.com.model.get;

import java.util.List;
import java.util.ArrayList;

public class AdvDetail extends Adv {

private int view_value;
private String ad_desc;
private String ad_meet_place;
private String ad_contact;
private String ad_bump;
private String ad_create_date;
private boolean ad_stat;
private String ad_dead_date;
private List<AdvImage> ad_images;
private int ctr_id;
private String ctr_name;

public AdvDetail() {
}

public String getAdContact(){
        return ad_contact;
}

public String getAdDesc(){
        return ad_desc;
}

public String getAdBump(){
        return ad_bump;
}

public String getAdCreateDate(){
        return ad_create_date;
}


public String getAdDeadDate(){
        return ad_dead_date;
}

public boolean getAdStat(){
        return ad_stat;
}

public List<AdvImage> getAdImages(){
        return ad_images;
}

public int getViewValue(){
        return view_value;
}

public int getCtrId(){
        return ctr_id;
}

public String getCtrName(){
        return ctr_name;
}

public void setAdDesc(String ad_desc){
        this.ad_desc = ad_desc;
}

public void setAdMeetPlace(String ad_meet_place){
        this.ad_meet_place = ad_meet_place;
}

public void setAdContact(String ad_contact){
        this.ad_contact = ad_contact;
}

public void setAdBump(String ad_bump){
        this.ad_bump = ad_bump;
}

public void setAdCreateDate(String ad_create_date){
        this.ad_create_date = ad_create_date;
}

public void setAdStat(boolean ad_stat){
        this.ad_stat = ad_stat;
}

public void setAdDeadDate(String ad_dead_date){
        this.ad_dead_date = ad_dead_date;
}

public void setAdImages(List<AdvImage> ad_images){
        this.ad_images = ad_images;
}

public void addImage(AdvImage ad_image){
        if(ad_images == null) {
                ad_images = new ArrayList();
        }
        ad_images.add(ad_image);
}

public void setViewValue(int view_value){
        this.view_value = view_value;
}

public void setCtrId(int ctr_id){
        this.ctr_id = ctr_id;
}

public void setCtrName(String ctr_name){
        this.ctr_name = ctr_name;
}

}
