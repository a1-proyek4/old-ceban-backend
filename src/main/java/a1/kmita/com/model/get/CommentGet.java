package a1.kmita.com.model.get;

public class CommentGet extends Comment {

private int cmt_id;
private String date;
private boolean status;


public CommentGet(){

}

public int getCmtId(){
        return cmt_id;
}

public void setCmtId(int cmt_id){
        this.cmt_id = cmt_id;
}

public String getCmtDate(){
        return date;
}

public boolean getCmtStatus(){
        return status;
}

public void setCmtDate(String date){
        this.date = date;
}

public void setCmtStatus(boolean status){
        this.status = status;
}

}
