package a1.kmita.com.model.get;

public class Inbox {

private String ibx_msg;
private String ibx_sdr_email;
private String ibx_sdr_name;

public Inbox(){

}

public String getIbxMsg(){
        return ibx_msg;
}

public String getIbxSdrEmail(){
        return ibx_msg;
}

public String getIbxSdrName(){
        return ibx_msg;
}

public void setIbxMsg(String ibx_msg){
  this.ibx_msg = ibx_msg;
}

public void setIbxSdrEmail(String ibx_sdr_email){
  this.ibx_sdr_email = ibx_sdr_email;
}

public void setIbxSdrName(String ibx_sdr_name){
  this.ibx_sdr_name = ibx_sdr_name;
}

}
