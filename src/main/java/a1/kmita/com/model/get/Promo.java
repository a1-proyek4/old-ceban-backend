package a1.kmita.com.model.get;

import a1.kmita.com.model.post.PromoPost;


public class Promo extends PromoPost {

private String promo_create_time;
private int promo_id;

public Promo(){

}

public int getPromoId(){
        return promo_id;
}

public String getPromoCreateTime(){
        return promo_create_time;
}

public void setPromoId(int promo_id){
        this.promo_id = promo_id;
}

public void setPromoCreateTime(String promo_create_time){
        this.promo_create_time = promo_create_time;
}


}
