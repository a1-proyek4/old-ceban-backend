package a1.kmita.com.model.get;

public class Notif{
    private Integer notif_id;
    private Integer slr_nim;
    private Boolean notif_read;
    private String notif_message;
    private String notif_time;

    public Notif(){

    }

    public Integer getSlrNim(){
        return slr_nim;
    }

    public void setSlrNim(Integer slr_nim){ 
        this.slr_nim = slr_nim;
    }

    public Integer getNotifId(){
        return notif_id;
    }

    public void setNotifId(Integer notif_id){
        this.notif_id = notif_id;
    }

    public Boolean getNotifRead(){
        return notif_read;
    }

    public void setNotifRead(Boolean notif_read){
        this.notif_read = notif_read;
    }

    public String getNotifMessage(){
        return notif_message;
    }

    public void setNotifMessage(String notif_message){
        this.notif_message = notif_message;
    }

    public String getNotifTime(){
        return notif_time;
    }

    public void setNotifTime(String notif_time){
        this.notif_time = notif_time;
    }

}