package a1.kmita.com.model.get;

public class Adv {

private int ad_id;
private String ad_title;
private int ad_price;
private String ad_image;
private int view_id;
private int slr_nim;
private String slr_name;
private String slr_department;


public Adv(){
}

public int getAdId(){
        return ad_id;
}

public int getAdPrice(){
        return ad_price;
}

public String getAdTitle(){
        return ad_title;
}

public String getAdImage(){
        return ad_image;
}

public int getSlrNim(){
        return slr_nim;
}

public String getSlrName(){
        return slr_name;
}

public String getSlrDepartment(){
        return slr_department;
}


public void setSlrNim(int slr_nim){
        this.slr_nim = slr_nim;
}

public void setSlrName(String slr_name){
        this.slr_name = slr_name;
}

public void setSlrDepartment(String slr_department){
        this.slr_department = slr_department;
}

public void setAdId(int ad_id){
        this.ad_id = ad_id;
}

public void setAdTitle(String ad_title){
        this.ad_title = ad_title;
}

public void setAdPrice(int ad_price){
        this.ad_price = ad_price;
}

public void setAdImage(String ad_image){
        this.ad_image = ad_image;
}
}
