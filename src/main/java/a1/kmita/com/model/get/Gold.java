package a1.kmita.com.model.get;

public class Gold extends Adv{

    private int gold_id;
    private int ad_id;
    private String gold_create_time;
    private String gold_end_time;
    
    public Gold(){
    }

    public int getAdId(){
        return ad_id;
    }

    public int getGoldId(){
        return gold_id;
    }

    public String getGoldCreateTime(){
        return gold_create_time;
    }

    public String getGoldEndTime(){
        return gold_end_time;
    }

    public void setAdId(int ad_id){
        this.ad_id = ad_id;
    }

    public void setGoldId(int gold_id){
        this.gold_id = gold_id;
    }

    public void setGoldCreateTime(String gold_create_time){
        this.gold_create_time = gold_create_time;
    }

    public void setGoldEndTime(String gold_end_time){
        this.gold_end_time = gold_end_time;
    }

}