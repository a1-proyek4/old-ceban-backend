package a1.kmita.com.model.get;

public class Seller {

private int slr_nim;
private String slr_name;
private String slr_department;
private String slr_studyprogram;
private boolean slr_gender;
private String slr_email;
private String slr_contact;
private String slr_image;
private String slr_pass;

public Seller(){
}

public int getSlrNim(){
  return slr_nim;
}

public void setSlrNim(int slr_nim){
  this.slr_nim = slr_nim;
}

public String getSlrName(){
  return slr_name;
}

public void setSlrName(String slr_name){
  this.slr_name = slr_name;
}

public String getSlrDepartment(){
  return slr_department;
}

public void setSlrDepartment(String slr_department){
  this.slr_department = slr_department;
}

public String getSlrStudyProgram(){
  return slr_studyprogram;
}

public void setSlrStudyProgram(String slr_studyprogram){
  this.slr_studyprogram = slr_studyprogram;
}

public boolean isSlrGender(){
  return slr_gender;
}

public void setSlrGender(boolean slr_gender){
  this.slr_gender = slr_gender;
}

public String getSlrContact(){
  return slr_contact;
}

public void setSlrContact(String slr_contact){
  this.slr_contact = slr_contact;
}

public String getSlrEmail(){
  return slr_email;
}

public void setSlrEmail(String slr_email){
  this.slr_email = slr_email;
}

public String getSlrImage(){
  return slr_image;
}

public void setSlrImage(String slr_image){
  this.slr_image = slr_image;
}

public String getSlrPass(){
  return slr_pass;
}

public void setSlrPass(String slr_pass){
  this.slr_pass = slr_pass;
}
}
