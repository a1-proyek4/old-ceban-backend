package a1.kmita.com.model.get;

public class Category {

private int ctr_id;
private String ctr_name;
private String ctr_image;
private String ctr_desc;
private int ctr_ad_count;

public Category() {
}


public int getCtrId(){
        return ctr_id;
}

public void setCtrId(int ctr_id) {
        this.ctr_id = ctr_id;
}

public int getCtrAdCount() {
        return ctr_ad_count;
}

public String getCtrName() {
        return ctr_name;
}

public String getCtrImage(){
        return ctr_image;
}

public String getCtrDesc(){
        return ctr_desc;
}

public void setCtrName(String ctr_name) {
        this.ctr_name = ctr_name;
}

public void setCtrImage(String ctr_image){
        this.ctr_image = ctr_image;
}

public void setCtrDesc(String ctr_desc) {
        this.ctr_desc = ctr_desc;
}

public void setCtrAdCount(int ctr_ad_count) {
        this.ctr_ad_count = ctr_ad_count;
}


}
