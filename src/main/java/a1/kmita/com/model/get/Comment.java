package a1.kmita.com.model.get;

public class Comment {

private String cmt_msg;
private String email;

public Comment() {
}

public String getCmtMsg(){
        return cmt_msg;
}

public String getCmtEmail(){
        return email;
}

public void setCmtMsg(String cmt_msg){
        this.cmt_msg = cmt_msg;
}

public void setCmtEmail(String email){
        this.email = email;
}

}
