package a1.kmita.com.model.get;

public class AdvImage{

    private String image;

    public AdvImage(String image){
      this.image = image;
    }

    public String getImage(){
        return image;
    }

    public void setImage(String image){
        this.image = image;
    }
}
