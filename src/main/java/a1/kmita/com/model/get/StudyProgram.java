package a1.kmita.com.model.get;

public class StudyProgram {

private int std_id;
private String std_name;
private int dpt_id;

public StudyProgram() {
}

public int getStudyProgramId(){
        return std_id;
}

public String getStudyProgramName(){
        return std_name;
}

public void setStudyProgramId(int std_id){
        this.std_id = std_id;
}

public void setStudyProgramName(String std_name){
        this.std_name = std_name;
}

public int getDptId(){
        return dpt_id;
}

public void setDptId(int dpt_id){
        this.dpt_id = dpt_id;
}


}