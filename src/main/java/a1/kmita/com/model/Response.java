package a1.kmita.com.model;

import a1.kmita.com.controller.DbHandler;

public class Response {

    private String status;
    private String message = "";

    public Response(){
    }

    public void setStatus(String status){
      this.status = status;
      setMessage(DbHandler.getMessage(Integer.parseInt(status)));
    }

    public String getStatus(){
      return status;
    }

    public void setMessage(String message){
      this.message = this.message + message;
    }

    public String getMessage(){
      return message;
    }
}
